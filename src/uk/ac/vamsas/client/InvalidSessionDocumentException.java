/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

/**
 * @author JimP
 * 
 */
public class InvalidSessionDocumentException extends Exception {
  /**
   * basic error for this exception
   */
  public static final String INVALID_DOC = "Invalid Shared Document.";

  /**
   * 
   */
  public InvalidSessionDocumentException() {
    super(INVALID_DOC);
  }

  /**
   * @param message
   */
  public InvalidSessionDocumentException(String message) {
    super(INVALID_DOC + ":" + message);
  }

  /**
   * @param cause
   */
  public InvalidSessionDocumentException(Throwable cause) {
    super(INVALID_DOC, cause);
  }

  /**
   * @param message
   * @param cause
   */
  public InvalidSessionDocumentException(String message, Throwable cause) {
    super(INVALID_DOC + ":" + message, cause);
  }

}
