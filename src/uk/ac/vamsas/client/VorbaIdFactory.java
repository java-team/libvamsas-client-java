/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.util.Hashtable;
import java.util.Vector;

/**
 * A VorbaIdFactory is constructed by an IClient instance. It guarantees that
 * any new VorbaId objects are unique within the VAMSAS session.
 * 
 * @author jimp
 */
public abstract class VorbaIdFactory implements IVorbaIdFactory {
  /**
   * hash of VorbaIds to Vobject.
   */
  protected Hashtable extantids = null;

  /**
   * hash of VorbaIds to persisted hash values
   */
  protected Hashtable extanthashv = null;

  /**
   * list of Vobjects added since last Document read.
   */
  protected Vector newobj = null;

  /**
   * flag indicating if unresolved references should be raised during UnMarshalling
   */
  protected boolean warnUnresolved;

  /**
   * construct a new id appropriate for this client in the vamsas session.
   * 
   * @return valid VorbaId for session, or null if VorbaIdFactory not configured
   *         correctly.
   */
  public abstract VorbaId makeVorbaId(Vobject vobject);

  /**
   * internal method to access the protected VorbaId object constructor This
   * shennanigan is to prevent casual generation of VorbaIds (which may destroy
   * the integrity of a Vamsas Document!)
   * 
   * @param id
   * @return new VorbaId object
   */
  protected VorbaId newId(String id) {
    return VorbaId.newId(id);
  }

  /**
   * Called by VorbaXmlBinder so the record of newly unmarshalled Vobject Ids is
   * accessible to the Document's VorbaIdFactory instance.
   * 
   * @param idtable
   */
  protected void setNewIdHash(Hashtable idtable) {
    extantids = idtable;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * uk.ac.vamsas.client.IVorbaIdFactory#updateHashValue(uk.ac.vamsas.client
   * .Vobject)
   */
  public void updateHashValue(Vobject vobject) {
    if (vobject.isRegisterable())
      extanthashv.put(vobject.getVorbaId(), new Vobjhash(vobject));
  }

  /**
   * TODO: decide if these are needed.
   * 
   * @param sessionHandle
   */
  protected abstract void setSession(SessionHandle sessionhandle);

  public abstract SessionHandle getSessionHandle();

  protected abstract void setClient(ClientHandle appHandle);

  public abstract ClientHandle getClientHandle();

  protected abstract void setUser(UserHandle userHandle);

  public abstract UserHandle getUserHandle();
}
