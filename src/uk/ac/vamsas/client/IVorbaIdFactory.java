/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

/**
 * @author jimp middleware interface for generating new VorbaId objects for a
 *         particular vamsas client based on the current session, user and
 *         client handle. Generally implemented by instances of the vamsas
 *         library api only.
 */
public interface IVorbaIdFactory {
  /**
   * construct a new id appropriate for this client in the vamsas session.
   * 
   * @param vobject
   *          TODO
   * 
   * @return valid VorbaId for session, or null if VorbaIdFactory not configured
   *         correctly.
   */
  public abstract VorbaId makeVorbaId(Vobject vobject);

  public abstract SessionHandle getSessionHandle();

  public abstract ClientHandle getClientHandle();

  public abstract UserHandle getUserHandle();

  /**
   * called when an object is touched by the vamsas library prior to writing to
   * record last hash for the object's VorbaId
   * 
   * @param vobject
   */
  public abstract void updateHashValue(Vobject vobject);
}
