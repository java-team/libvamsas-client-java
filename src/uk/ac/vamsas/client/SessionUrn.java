/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.net.URI;
import java.util.Hashtable;

/**
 * @author jimp base class for vamsas session/document types uses java.net.URI
 *         internally for construction of URN
 */
public abstract class SessionUrn {
  protected URI urn;

  /**
   * The types of URI protocols we understand
   */
  protected static final Hashtable TYPES = new Hashtable();

  protected SessionUrn() {
    //
  }

  /**
   * construct urn for a locally stored session file
   * 
   * @param type
   * @param url
   */
  protected SessionUrn(String type, java.net.URL url) {
    if (!TYPES.containsKey(type.toLowerCase()))
      throw new Error("Unknown " + this.getClass().getName() + " type '" + type
          + "' for URL '" + url + "'");
    try {

      this.setURN(type + "://" + url.getPath());
      // urn = URI.create(type+"://"+url.getPath());
    } catch (Exception e) {
      // TODO: something better than throwing an error should be done here.
      e.printStackTrace(System.err);
      throw new Error(e);
    }
  }

  protected SessionUrn(String type, URI uri) {
    if (!TYPES.containsKey(type.toLowerCase()))
      throw new Error("Unknown " + this.getClass().getName() + " type '" + type
          + "' for URI '" + uri + "'");
    try {
      // this.setURN(type+"://"+uri.getPath());
      // bad hack but should do the trick
      this.setURN(type + "://" + uri.getRawPath());
    } catch (Exception e) {
      // TODO: something better than throwing an error should be done here.
      e.printStackTrace(System.err);
      throw new Error(e);
    }
  }

  public String getSessionUrn() {
    return this.urn.toString();
  }

  /**
   * Set the urn attribute create a URI from the provided String
   * 
   * @param urnString
   *          urn to convert to a URN
   */
  protected void setURN(String urnString) throws InvalidSessionUrnException// NullPointerException,
                                                                           // IllegalArgumentException
  {
    try {
      this.urn = URI.create(urnString);
    } catch (Exception e) {
      throw new InvalidSessionUrnException(e);
    }
  }

}
