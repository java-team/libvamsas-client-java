/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

/**
 * Holds information about Vobj instances that is held over between different
 * writes to the document. Currently records the LhashValue obtained from
 * Vobject
 * 
 * @author JimP
 * 
 */
public class Vobjhash {
  private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
      .getLog(Vobjhash.class);

  long hashvalue;

  /**
   * creates a record of the hash value for a vamsas document object
   * 
   * @param tobemarshalled
   */
  public Vobjhash(Vobject tobemarshalled) {
    tobemarshalled.doHash();
    hashvalue = tobemarshalled.get__last_hash();
    if (log.isDebugEnabled())
      log.debug(tobemarshalled.getVorbaId() + ":" + hashvalue);
  }

  /**
   * compares the old hash value with the unmarshalled object's most recently
   * computed hash value and updates internal record.
   * 
   * @param unmarshalled
   * @return true if the hash values differ
   */
  public boolean isUpdated(Vobject unmarshalled) {
    long oldhash = hashvalue;
    unmarshalled.doHash();
    hashvalue = unmarshalled.get__last_hash();
    if (log.isDebugEnabled() && oldhash != hashvalue)
      log.debug(" has changed." + unmarshalled.getVorbaId());
    return oldhash != hashvalue;
  }
}
