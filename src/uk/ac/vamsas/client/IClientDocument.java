/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import uk.ac.vamsas.objects.core.VAMSAS;

/**
 * Defines the API for the Vamsas XML Document as accessed by a Vamsas
 * SimpleClient Application. An instance of this interface is valid for a
 * particular set of user, session and application handles.
 * 
 * It initially represents a snapshot of the XML document at a particular time -
 * queriable by reference or by retrieval of root objects. It provides methods
 * to make new Vobject references, These are guaranteed to be unique amongst
 * existing objects in the document, all other references created by this
 * Vobject's instance and all other references constructed by any other vamsas
 * agents in the session. TODO: LATER: finegrained access control for
 * public/private user access Finegrained access control: Since a clientDocument
 * is created for a particular UserHandle, there is scope for fine grain data
 * access control based on user identity. A user may also want to make private
 * notes, not available to other people using the same application in the same
 * session. TODO: LATER: implement a more sophisticated query interface for
 * quickly identifying new data in a vamsas document and manipulating existing
 * objects
 * 
 * @author jimp
 */
public interface IClientDocument {

  /**
   * Get a single Vobject.
   * 
   * @param id
   * @return Vobject referred to by id or null if it doesn't exist.
   */
  Vobject getObject(VorbaId id);

  /**
   * Get a list of objects.
   * 
   * @param ids
   * @return array of objects using a vector of VorbaId ids.
   */
  Vobject[] getObjects(VorbaId[] ids);

  /**
   * Returns all root objects in document. All objects inherit from
   * uk.ac.vamsas.client.Vobject and have valid VorbaIds and provenance entries.
   * 
   * @return array of root Vamsas element objects.
   */
  VAMSAS[] getVamsasRoots();

  /**
   * set the VAMSAS roots in the document TODO: decide if objects are verified
   * for provenance and VorbaIds by this call or when document is stored TODO:
   * decide if this call should throw InvalidVamsasObject exceptions. TODO:
   * decide how this call deals with applications that 'forget' to include all
   * VAMSAS roots (this is where reference counting/garbage collection happens)
   * 
   * @param roots
   */
  void setVamsasRoots(VAMSAS[] roots);

  /**
   * Adds a new VAMSAS root entry TODO: decide on same InvalidVamsasObject
   * exceptions. TODO: decide if a 'removeVamsasRoot' method is really needed.
   * 
   * @param newroot
   */
  void addVamsasRoot(VAMSAS newroot);

  /**
   * Returns an Vobject with a valid VorbaId, and provenance element. The
   * VorbaId is so the application may refer to it in its own dataspace.
   * 
   * Note: An Vobject with valid VorbaId will not be reregistered. Advice:
   * Calling this method for a high-level Vobject (such as
   * uk.ac.vamsas.objects.core.VAMSAS) will register all its component objects
   * too.
   * 
   * @param unregistered
   *          unregistered vamsas Vobject
   * @return VorbaId registered for vamsas Vobject
   */
  VorbaId registerObject(Vobject unregistered);

  /**
   * Returns an array of objects, each with a valid VorbaId (and completed
   * provenance entry). Note: An Vobject with valid VorbaId will not be
   * reregistered.
   * 
   * @param unregistered
   *          array of unregistered objects.
   * @return array of VorbaIds for the registered objects
   */
  VorbaId[] registerObjects(Vobject[] unregistered);

  /**
   * Get instance of Client and User specific vamsas document data access
   * interface.
   * 
   * @return Interface to Client and user specific application data
   */
  IClientAppdata getClientAppdata();
}
