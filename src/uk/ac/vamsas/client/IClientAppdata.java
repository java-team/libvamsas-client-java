/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

/**
 * Object for accessing Client and User specific data in an IClientDocument
 * instance.
 * 
 */
public interface IClientAppdata {
  /**
   * @return true if Client's non-user specific application data is non-zero
   *         length.
   */
  boolean hasClientAppdata();

  /**
   * @return true if User's Client Application data is non-zero length
   */
  boolean hasUserAppdata();

  /**
   * 
   * @return byte array containing the Client's non-user specific application
   *         data
   */
  byte[] getClientAppdata();

  /**
   * 
   * @return byte array containing the Client's user specific application data
   */
  byte[] getUserAppdata();

  /**
   * set the non-User-specific application data
   * 
   * @param data
   *          - the new non-user-specific data
   */
  void setClientAppdata(byte[] data);

  /**
   * set the User-specific application data
   * 
   * @param data
   *          - the new user-specific data
   */
  void setUserAppdata(byte[] data);

  /**
   * @return non-user specific data output stream
   */
  AppDataOutputStream getClientOutputStream();

  /**
   * @return non-user specific data input stream
   */
  AppDataInputStream getClientInputStream();

  /**
   * 
   * @return user specific data output stream
   */
  AppDataOutputStream getUserOutputStream();

  /**
   * 
   * @return user specific data input stream
   */
  AppDataInputStream getUserInputStream();
}
