/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client;

import java.util.Hashtable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import uk.ac.vamsas.objects.core.VAMSAS;

/**
 * skeleton abstract class to allow client implementations access to
 * vamsas.client.Vobject registry mechanism.
 */
public abstract class ClientDocument implements IClientDocument {
  static Log log = LogFactory.getLog(ClientDocument.class);

  /**
   * collection of uk.ac.vamsas.client.Vobject references
   */
  protected Hashtable vamsasObjects;

  protected IVorbaIdFactory vorbafactory;

  protected ClientDocument(Hashtable objects, IVorbaIdFactory factory) {
    vamsasObjects = objects;
    vorbafactory = factory;
  }

  /**
   * @see IClientHandle.registerObject(Vobject unregistered)
   */
  protected VorbaId _registerObject(Vobject unregistered) {
    // be ultra safe here because the user may be trying to mix different
    // factories
    if (unregistered.__vorba == null)
      unregistered.__vorba = vorbafactory;
    else if (unregistered.__vorba != vorbafactory) {
      // LATER: decide if this is allowed - it isn't for the moment.
      log
          .error("Attempt to overwrite info in a registered vorba Vobject (under a different IVorbaIdFactory) ! - Implementation fix needed.");
      return null;
    } else {
      // probably didn't need to call registerObject.
      log.debug("Redundant call to registerObject");
    }
    // TODO: add default provenance.
    // TODO: decide if we need to do call __ensure_instance_ids here
    // TODO: check if __ensure_instance_ids works correctly with new 'visit
    // flag' mechanism
    unregistered.__ensure_instance_ids(); // call cascade method here :
    return unregistered.getVorbaId();
  }
}
