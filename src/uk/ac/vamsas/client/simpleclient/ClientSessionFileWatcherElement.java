/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

public class ClientSessionFileWatcherElement extends SessionFileWatcherElement {

  private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory
      .getLog(VamsasFileWatcherElement.class);

  /**
   * count of watch cycle before considering there is no other active client.
   */
  private int timeoutBeforeLastCycle = -1;

  private boolean isTimeOutEnable = false;

  /**
   * @param watcher
   * @param handler
   */
  public ClientSessionFileWatcherElement(SessionFile watcher,
      WatcherCallBack handler) {
    super(watcher, handler);
  }

  /**
   * @param watcher
   * @param handler
   * @param enableWatching
   */
  public ClientSessionFileWatcherElement(SessionFile watcher,
      WatcherCallBack handler, boolean enableWatching) {
    super(watcher, handler, enableWatching);
  }

  /**
   * @see uk.ac.vamsas.client.simpleclient.WatcherElement#doWatch() * @return
   *      true if the handler was called for a changeEvent
   */
  public boolean doWatch() {
    if (!watchForChange || handler == null) { // log.debug("!watchForChange || handler==null");
      return false;
    }
    if (watcher == null)
      initWatch(); // somehow not done the first time
    handlerCalled = false;
    Lock doclock = null;
    try {
      doclock = watcher.getChangedState();
    } catch (Exception e) {
      log.error("Whilst watching " + watcher.getSubject(), e);
    }
    // log.debug("got lock watcher");
    if (doclock == null) {// no change detected
      this.cycleCountSinceModif++;
      if (this.isTimeOutEnable && cycleCountSinceModif > timeoutBeforeLastCycle) {
        if (this.handler != null)
          synchronized (this.handler) {
            this.callHandler(doclock);
          }
      }
      // log.debug("no modification");
      return false;
    }
    if (this.isTimeOutEnable)
      this.cycleCountSinceModif = 0; // change detected
    if (this.handler != null)
      synchronized (this.handler) {
        this.callHandler(doclock);
      }

    return true;
  }

  /**
   * count of cycles since last modification on the file
   */
  protected int cycleCountSinceModif = 0;

  /**
   * resets count of watch cycles (default value : 0)
   * 
   */
  protected void resetCycleCount() {
    this.cycleCountSinceModif = 0;
  }

  /**
   * Increases the count of cycles
   * 
   */
  protected void increaseCycleCount() {
    this.cycleCountSinceModif++;
  }

  /**
   * Enable the time out if the timeout is greater than zero
   * 
   * @param timeoutBeforeLastCycle
   *          the timeoutBeforeLastCycle to set
   */
  public void setTimeoutBeforeLastCycle(int timeoutBeforeLastCycle) {

    this.timeoutBeforeLastCycle = timeoutBeforeLastCycle;
    if (this.timeoutBeforeLastCycle > 0)
      isTimeOutEnable = true;
  }

  /**
   * Disables the checking on the count of cycles
   * 
   */
  public void disableCycleTimeOut() {
    this.isTimeOutEnable = false;
  }
}
