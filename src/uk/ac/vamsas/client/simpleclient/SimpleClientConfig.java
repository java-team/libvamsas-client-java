/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

/**
 * Config for SimpleClient - a basic Vamsas Client Interface implementation.
 * 
 * @author JimP
 * 
 */
public class SimpleClientConfig {
  public boolean _validatemergedroots = false;

  public boolean _validateupdatedroots = false;

  /**
   * New VAMSAS roots added (merged) into the document will be validated before
   * actually being added.
   * 
   * @return
   */
  public boolean validateMergedRoots() {
    return _validatemergedroots;
  }

  /**
   * existing VAMSAS roots that may have been modified will be validated before
   * the updated document can be written.
   * 
   * @return
   */
  public boolean validateUpdatedRoots() {
    return _validateupdatedroots;
  }

}
