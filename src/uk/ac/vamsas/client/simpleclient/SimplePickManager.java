/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

import org.apache.commons.logging.*;

import uk.ac.vamsas.client.picking.IMessageHandler;
import uk.ac.vamsas.client.picking.IPickManager;
import uk.ac.vamsas.client.picking.Message;
import uk.ac.vamsas.client.picking.SocketManager;

/**
 * Blocking message handler
 * 
 * @author
 * 
 */
public class SimplePickManager implements IPickManager {
  private Log log = LogFactory.getLog(SimplePickManager.class);

  SocketManager manager = null;

  SimplePickManager(SocketManager manager) {
    this.manager = manager;
    final SimplePickManager me = this;
    /**
     * add a handler that calls the SimplePickManager message handler
     */
    manager.registerMessageHandler(new IMessageHandler() {
      public void handleMessage(Message message) {
        me.handleMessage(message);
      }
    });

  }

  /**
   * when false, messages are queued in a FIFO until thread can restart.
   */
  private boolean passThru = true;

  /**
   * internal flag - set to true to sleep until passThru is true before passing
   * on a message
   */
  private boolean qUEUE = false;

  /**
   * the client apps message handler
   */
  IMessageHandler pickHandler = null;

  public void registerMessageHandler(IMessageHandler handler) {
    pickHandler = handler;

  }

  public synchronized void sendMessage(Message message) {
    // throw away messages whilst we block
    if (passThru && manager != null)
      manager.sendMessage(message);
  }

  /**
   * pass message onto the Apps handler, or wait until passThru is true before
   * passing message on.
   * 
   * @param message
   */
  protected synchronized void handleMessage(Message message) {
    if (qUEUE) {
      while (!passThru && manager != null) {
        log.debug("Not passing through.");
        try {
          Thread.sleep(5);
        } catch (InterruptedException e) {
        }
        ;
      }
    }
    if (passThru && manager != null)
      pickHandler.handleMessage(message);
  }

  /**
   * @return true if messages are being passed to handlers
   */
  public boolean isPassThru() {
    return passThru;
  }

  /**
   * @param true to pass messages on to handlers, false to hold messages in
   *        queue
   */
  public void setPassThru(boolean passThru) {
    this.passThru = passThru;
  }

  /**
   * shutdown the pickmanager and remove all references to it
   */
  public void shutdown() {
    passThru = false;
    manager.registerMessageHandler(null);
    SocketManager dying = manager;
    manager = null;
    dying.shutdown();
  }
}
