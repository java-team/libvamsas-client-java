/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.client.simpleclient;

/**
 * Element of the VamsasFileWatcherThread event generator chain. Keep a
 * reference to this element before adding it to the chain in order to control
 * the generation of events with halt and enable.
 * 
 * doWatch will do nothing if the element is not enabled.
 * 
 */
public class SessionFileWatcherElement extends WatcherElement {
  SessionFile watched = null;

  /**
   * create a new, enabled watch element
   * 
   * @param watcher
   *          file being watched
   * @param handler
   *          handler to call on state change
   */
  public SessionFileWatcherElement(SessionFile watcher, WatcherCallBack handler) {
    this(watcher, handler, true);
  }

  protected void initWatch() {
    if (watched == null)
      return;
    if (watcher == null || !watcher.getSubject().equals(watched.sessionFile))
      watcher = new FileWatcher(watched.sessionFile);
    else
      watcher.setState();
  }

  protected void endWatch() {
    // leaves watcher in state its in.
  }

  /**
   * new watcher with flag to initially skip watching this sessionfile
   * 
   * @param watcher
   * @param handler
   * @param enableWatching
   */
  public SessionFileWatcherElement(SessionFile watcher,
      WatcherCallBack handler, boolean enableWatching) {
    super(handler);
    this.watched = watcher;
    if (enableWatching)
      enableWatch();
    else
      haltWatch();
  }

  /**
   * @return the watched
   */
  public SessionFile getWatched() {
    return watched;
  }

  protected String getSubject() {
    return watched.sessionFile.toString();
  }
}
