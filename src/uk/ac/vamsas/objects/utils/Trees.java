/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.utils;

import java.io.IOException;

import uk.ac.vamsas.objects.core.Tree;
import uk.ac.vamsas.objects.utils.trees.NewickFile;

public class Trees {

  /**
   * TODO: implement helper functions based on NewickFile tree parser
   * 
   * 1. create a newickfile object from a newick string. 2. let user map a
   * vamsas object to each node by setting that node's
   * ((SequenceNode)node).setElement(VorbaId) 3. generate Tree object with
   * newickString as a newick string, and added treenodes to record mapping to
   * vorba ids
   */

}
