/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * base type for citing arbitrary links between vamsas objects Optional human
 * readable description of the relationship
 * 
 * 
 * @version $Revision$ $Date$
 */
public class ReferenceType extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * internal content storage
   */
  private java.lang.String _content = "";

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * List of one or more vamsas object references
   */
  private java.util.Vector _refs;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public ReferenceType() {
    super();
    setContent("");
    this._refs = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vRefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addRefs(final java.lang.Object vRefs)
      throws java.lang.IndexOutOfBoundsException {
    this._refs.addElement(vRefs);
  }

  /**
   * 
   * 
   * @param index
   * @param vRefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addRefs(final int index, final java.lang.Object vRefs)
      throws java.lang.IndexOutOfBoundsException {
    this._refs.add(index, vRefs);
  }

  /**
   * Method enumerateRefs.
   * 
   * @return an Enumeration over all java.lang.Object elements
   */
  public java.util.Enumeration enumerateRefs() {
    return this._refs.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof ReferenceType) {

      ReferenceType temp = (ReferenceType) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._content != null) {
        if (temp._content == null)
          return false;
        if (this._content != temp._content) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._content);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._content);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._content.equals(temp._content)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
          }
        }
      } else if (temp._content != null)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._refs != null) {
        if (temp._refs == null)
          return false;
        if (this._refs != temp._refs) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._refs);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._refs);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._refs);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._refs);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._refs.equals(temp._refs)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._refs);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._refs);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._refs);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._refs);
          }
        }
      } else if (temp._refs != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @return the value of field 'Content'.
   */
  public java.lang.String getContent() {
    return this._content;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Method getRefs.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the java.lang.Object at the given index
   */
  public java.lang.Object getRefs(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._refs.size()) {
      throw new IndexOutOfBoundsException("getRefs: Index value '" + index
          + "' not in range [0.." + (this._refs.size() - 1) + "]");
    }

    return _refs.get(index);
  }

  /**
   * Method getRefs.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public java.lang.Object[] getRefs() {
    java.lang.Object[] array = new java.lang.Object[0];
    return (java.lang.Object[]) this._refs.toArray(array);
  }

  /**
   * Method getRefsAsReference.Returns a reference to '_refs'. No type checking
   * is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getRefsAsReference() {
    return this._refs;
  }

  /**
   * Method getRefsCount.
   * 
   * @return the size of this collection
   */
  public int getRefsCount() {
    return this._refs.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_content != null
        && !org.castor.util.CycleBreaker.startingToCycle(_content)) {
      result = 37 * result + _content.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_content);
    }
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_refs != null && !org.castor.util.CycleBreaker.startingToCycle(_refs)) {
      result = 37 * result + _refs.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_refs);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllRefs() {
    this._refs.clear();
  }

  /**
   * Method removeRefs.
   * 
   * @param vRefs
   * @return true if the object was removed from the collection.
   */
  public boolean removeRefs(final java.lang.Object vRefs) {
    boolean removed = _refs.remove(vRefs);
    return removed;
  }

  /**
   * Method removeRefsAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public java.lang.Object removeRefsAt(final int index) {
    java.lang.Object obj = this._refs.remove(index);
    return obj;
  }

  /**
   * Sets the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @param content
   *          the value of field 'content'.
   */
  public void setContent(final java.lang.String content) {
    this._content = content;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * 
   * 
   * @param index
   * @param vRefs
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setRefs(final int index, final java.lang.Object vRefs)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._refs.size()) {
      throw new IndexOutOfBoundsException("setRefs: Index value '" + index
          + "' not in range [0.." + (this._refs.size() - 1) + "]");
    }

    this._refs.set(index, vRefs);
  }

  /**
   * 
   * 
   * @param vRefsArray
   */
  public void setRefs(final java.lang.Object[] vRefsArray) {
    // -- copy array
    _refs.clear();

    for (int i = 0; i < vRefsArray.length; i++) {
      this._refs.add(vRefsArray[i]);
    }
  }

  /**
   * Sets the value of '_refs' by copying the given Vector. All elements will be
   * checked for type safety.
   * 
   * @param vRefsList
   *          the Vector to copy.
   */
  public void setRefs(final java.util.Vector vRefsList) {
    // copy vector
    this._refs.clear();

    this._refs.addAll(vRefsList);
  }

  /**
   * Sets the value of '_refs' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param refsVector
   *          the Vector to set.
   */
  public void setRefsAsReference(final java.util.Vector refsVector) {
    this._refs = refsVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.ReferenceType
   */
  public static uk.ac.vamsas.objects.core.ReferenceType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.ReferenceType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.ReferenceType.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
