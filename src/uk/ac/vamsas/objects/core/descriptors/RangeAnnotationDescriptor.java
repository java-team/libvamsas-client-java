/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core.descriptors;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import uk.ac.vamsas.objects.core.RangeAnnotation;

/**
 * Class RangeAnnotationDescriptor.
 * 
 * @version $Revision$ $Date$
 */
public class RangeAnnotationDescriptor extends
    uk.ac.vamsas.objects.core.descriptors.RangeTypeDescriptor {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _elementDefinition.
   */
  private boolean _elementDefinition;

  /**
   * Field _nsPrefix.
   */
  private java.lang.String _nsPrefix;

  /**
   * Field _nsURI.
   */
  private java.lang.String _nsURI;

  /**
   * Field _xmlName.
   */
  private java.lang.String _xmlName;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public RangeAnnotationDescriptor() {
    super();
    setExtendsWithoutFlatten(new uk.ac.vamsas.objects.core.descriptors.RangeTypeDescriptor());
    _nsURI = "http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes";
    _xmlName = "rangeAnnotation";
    _elementDefinition = false;

    // -- set grouping compositor
    setCompositorAsSequence();
    org.exolab.castor.xml.util.XMLFieldDescriptorImpl desc = null;
    org.exolab.castor.mapping.FieldHandler handler = null;
    org.exolab.castor.xml.FieldValidator fieldValidator = null;
    // -- initialize attribute descriptors

    // -- _id
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_id", "id",
        org.exolab.castor.xml.NodeType.Attribute);
    super.setIdentity(desc);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getId();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setId((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new java.lang.String();
      }
    };
    desc.setHandler(handler);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _id
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.IdValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.IdValidator();
      fieldValidator.setValidator(typeValidator);
    }
    desc.setValidator(fieldValidator);
    // -- _modifiable
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_modifiable", "modifiable",
        org.exolab.castor.xml.NodeType.Attribute);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getModifiable();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setModifiable((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _modifiable
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _group
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_group", "group",
        org.exolab.castor.xml.NodeType.Attribute);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getGroup();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setGroup((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _group
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _type
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_type", "type",
        org.exolab.castor.xml.NodeType.Attribute);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getType();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setType((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setRequired(true);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _type
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(1);
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- initialize element descriptors

    // -- _label
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_label", "label",
        org.exolab.castor.xml.NodeType.Element);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getLabel();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setLabel((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _label
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _description
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_description", "description",
        org.exolab.castor.xml.NodeType.Element);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getDescription();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setDescription((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _description
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _status
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_status", "status",
        org.exolab.castor.xml.NodeType.Element);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getStatus();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.setStatus((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _status
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- _annotationElementList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.AnnotationElement.class,
        "_annotationElementList", "annotationElement",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getAnnotationElement();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target
              .addAnnotationElement((uk.ac.vamsas.objects.core.AnnotationElement) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.removeAllAnnotationElement();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.AnnotationElement();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _annotationElementList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _scoreList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Score.class, "_scoreList", "score",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getScore();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.addScore((uk.ac.vamsas.objects.core.Score) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.removeAllScore();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Score();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _scoreList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _linkList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Link.class, "_linkList", "link",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getLink();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.addLink((uk.ac.vamsas.objects.core.Link) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.removeAllLink();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Link();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _linkList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _propertyList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Property.class, "_propertyList", "property",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        RangeAnnotation target = (RangeAnnotation) object;
        return target.getProperty();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.addProperty((uk.ac.vamsas.objects.core.Property) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          RangeAnnotation target = (RangeAnnotation) object;
          target.removeAllProperty();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Property();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _propertyList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Method getAccessMode.
   * 
   * @return the access mode specified for this class.
   */
  public org.exolab.castor.mapping.AccessMode getAccessMode() {
    return null;
  }

  /**
   * Method getIdentity.
   * 
   * @return the identity field, null if this class has no identity.
   */
  public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
    return super.getIdentity();
  }

  /**
   * Method getJavaClass.
   * 
   * @return the Java class represented by this descriptor.
   */
  public java.lang.Class getJavaClass() {
    return uk.ac.vamsas.objects.core.RangeAnnotation.class;
  }

  /**
   * Method getNameSpacePrefix.
   * 
   * @return the namespace prefix to use when marshaling as XML.
   */
  public java.lang.String getNameSpacePrefix() {
    return _nsPrefix;
  }

  /**
   * Method getNameSpaceURI.
   * 
   * @return the namespace URI used when marshaling and unmarshaling as XML.
   */
  public java.lang.String getNameSpaceURI() {
    return _nsURI;
  }

  /**
   * Method getValidator.
   * 
   * @return a specific validator for the class described by this
   *         ClassDescriptor.
   */
  public org.exolab.castor.xml.TypeValidator getValidator() {
    return this;
  }

  /**
   * Method getXMLName.
   * 
   * @return the XML Name for the Class being described.
   */
  public java.lang.String getXMLName() {
    return _xmlName;
  }

  /**
   * Method isElementDefinition.
   * 
   * @return true if XML schema definition of this Class is that of a global
   *         element or element with anonymous type definition.
   */
  public boolean isElementDefinition() {
    return _elementDefinition;
  }

}
