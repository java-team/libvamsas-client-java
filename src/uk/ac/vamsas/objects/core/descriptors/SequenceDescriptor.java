/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core.descriptors;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import uk.ac.vamsas.objects.core.Sequence;

/**
 * Class SequenceDescriptor.
 * 
 * @version $Revision$ $Date$
 */
public class SequenceDescriptor extends
    uk.ac.vamsas.objects.core.descriptors.SequenceTypeDescriptor {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _elementDefinition.
   */
  private boolean _elementDefinition;

  /**
   * Field _nsPrefix.
   */
  private java.lang.String _nsPrefix;

  /**
   * Field _nsURI.
   */
  private java.lang.String _nsURI;

  /**
   * Field _xmlName.
   */
  private java.lang.String _xmlName;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public SequenceDescriptor() {
    super();
    setExtendsWithoutFlatten(new uk.ac.vamsas.objects.core.descriptors.SequenceTypeDescriptor());
    _nsURI = "http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes";
    _xmlName = "Sequence";
    _elementDefinition = true;

    // -- set grouping compositor
    setCompositorAsSequence();
    org.exolab.castor.xml.util.XMLFieldDescriptorImpl desc = null;
    org.exolab.castor.mapping.FieldHandler handler = null;
    org.exolab.castor.xml.FieldValidator fieldValidator = null;
    // -- initialize attribute descriptors

    // -- _id
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_id", "id",
        org.exolab.castor.xml.NodeType.Attribute);
    super.setIdentity(desc);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        Sequence target = (Sequence) object;
        return target.getId();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.setId((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new java.lang.String();
      }
    };
    desc.setHandler(handler);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _id
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    { // -- local scope
      org.exolab.castor.xml.validators.IdValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.IdValidator();
      fieldValidator.setValidator(typeValidator);
    }
    desc.setValidator(fieldValidator);
    // -- _dictionary
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        java.lang.String.class, "_dictionary", "dictionary",
        org.exolab.castor.xml.NodeType.Attribute);
    desc.setImmutable(true);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        Sequence target = (Sequence) object;
        return target.getDictionary();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.setDictionary((java.lang.String) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return null;
      }
    };
    desc.setHandler(handler);
    desc.setRequired(true);
    desc.setMultivalued(false);
    addFieldDescriptor(desc);

    // -- validation code for: _dictionary
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(1);
    { // -- local scope
      org.exolab.castor.xml.validators.StringValidator typeValidator;
      typeValidator = new org.exolab.castor.xml.validators.StringValidator();
      fieldValidator.setValidator(typeValidator);
      typeValidator.setWhiteSpace("preserve");
    }
    desc.setValidator(fieldValidator);
    // -- initialize element descriptors

    // -- _dbRefList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.DbRef.class, "_dbRefList", "dbRef",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        Sequence target = (Sequence) object;
        return target.getDbRef();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.addDbRef((uk.ac.vamsas.objects.core.DbRef) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.removeAllDbRef();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.DbRef();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _dbRefList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
    // -- _vxrefList
    desc = new org.exolab.castor.xml.util.XMLFieldDescriptorImpl(
        uk.ac.vamsas.objects.core.Vxref.class, "_vxrefList", "vxref",
        org.exolab.castor.xml.NodeType.Element);
    handler = new org.exolab.castor.xml.XMLFieldHandler() {
      public java.lang.Object getValue(java.lang.Object object)
          throws IllegalStateException {
        Sequence target = (Sequence) object;
        return target.getVxref();
      }

      public void setValue(java.lang.Object object, java.lang.Object value)
          throws IllegalStateException, IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.addVxref((uk.ac.vamsas.objects.core.Vxref) value);
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public void resetValue(Object object) throws IllegalStateException,
          IllegalArgumentException {
        try {
          Sequence target = (Sequence) object;
          target.removeAllVxref();
        } catch (java.lang.Exception ex) {
          throw new IllegalStateException(ex.toString());
        }
      }

      public java.lang.Object newInstance(java.lang.Object parent) {
        return new uk.ac.vamsas.objects.core.Vxref();
      }
    };
    desc.setHandler(handler);
    desc.setNameSpaceURI("http://www.vamsas.ac.uk/schemas/1.0/vamsasTypes");
    desc.setMultivalued(true);
    addFieldDescriptor(desc);

    // -- validation code for: _vxrefList
    fieldValidator = new org.exolab.castor.xml.FieldValidator();
    fieldValidator.setMinOccurs(0);
    { // -- local scope
    }
    desc.setValidator(fieldValidator);
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Method getAccessMode.
   * 
   * @return the access mode specified for this class.
   */
  public org.exolab.castor.mapping.AccessMode getAccessMode() {
    return null;
  }

  /**
   * Method getIdentity.
   * 
   * @return the identity field, null if this class has no identity.
   */
  public org.exolab.castor.mapping.FieldDescriptor getIdentity() {
    return super.getIdentity();
  }

  /**
   * Method getJavaClass.
   * 
   * @return the Java class represented by this descriptor.
   */
  public java.lang.Class getJavaClass() {
    return uk.ac.vamsas.objects.core.Sequence.class;
  }

  /**
   * Method getNameSpacePrefix.
   * 
   * @return the namespace prefix to use when marshaling as XML.
   */
  public java.lang.String getNameSpacePrefix() {
    return _nsPrefix;
  }

  /**
   * Method getNameSpaceURI.
   * 
   * @return the namespace URI used when marshaling and unmarshaling as XML.
   */
  public java.lang.String getNameSpaceURI() {
    return _nsURI;
  }

  /**
   * Method getValidator.
   * 
   * @return a specific validator for the class described by this
   *         ClassDescriptor.
   */
  public org.exolab.castor.xml.TypeValidator getValidator() {
    return this;
  }

  /**
   * Method getXMLName.
   * 
   * @return the XML Name for the Class being described.
   */
  public java.lang.String getXMLName() {
    return _xmlName;
  }

  /**
   * Method isElementDefinition.
   * 
   * @return true if XML schema definition of this Class is that of a global
   *         element or element with anonymous type definition.
   */
  public boolean isElementDefinition() {
    return _elementDefinition;
  }

}
