/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Two sets of ranges defined between objects - usually sequences, indicating
 * which regions on each are mapped.
 * 
 * @version $Revision$ $Date$
 */
public class MapType extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
     */
  private uk.ac.vamsas.objects.core.Local _local;

  /**
     */
  private uk.ac.vamsas.objects.core.Mapped _mapped;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public MapType() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof MapType) {

      MapType temp = (MapType) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._local != null) {
        if (temp._local == null)
          return false;
        if (this._local != temp._local) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._local);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._local);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._local);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._local);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._local.equals(temp._local)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._local);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._local);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._local);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._local);
          }
        }
      } else if (temp._local != null)
        return false;
      if (this._mapped != null) {
        if (temp._mapped == null)
          return false;
        if (this._mapped != temp._mapped) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._mapped);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._mapped);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._mapped);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapped);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._mapped.equals(temp._mapped)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._mapped);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapped);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._mapped);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._mapped);
          }
        }
      } else if (temp._mapped != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'local'.
   * 
   * @return the value of field 'Local'.
   */
  public uk.ac.vamsas.objects.core.Local getLocal() {
    return this._local;
  }

  /**
   * Returns the value of field 'mapped'.
   * 
   * @return the value of field 'Mapped'.
   */
  public uk.ac.vamsas.objects.core.Mapped getMapped() {
    return this._mapped;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_local != null && !org.castor.util.CycleBreaker.startingToCycle(_local)) {
      result = 37 * result + _local.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_local);
    }
    if (_mapped != null
        && !org.castor.util.CycleBreaker.startingToCycle(_mapped)) {
      result = 37 * result + _mapped.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_mapped);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'local'.
   * 
   * @param local
   *          the value of field 'local'.
   */
  public void setLocal(final uk.ac.vamsas.objects.core.Local local) {
    this._local = local;
  }

  /**
   * Sets the value of field 'mapped'.
   * 
   * @param mapped
   *          the value of field 'mapped'.
   */
  public void setMapped(final uk.ac.vamsas.objects.core.Mapped mapped) {
    this._mapped = mapped;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.MapType
   */
  public static uk.ac.vamsas.objects.core.MapType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.MapType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.MapType.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
