/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class DataSetAnnotations.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class DataSetAnnotations extends
    uk.ac.vamsas.objects.core.RangeAnnotation implements java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * annotation is associated with a particular dataset sequence
   */
  private java.util.Vector _seqRef;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public DataSetAnnotations() {
    super();
    this._seqRef = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vSeqRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSeqRef(final java.lang.Object vSeqRef)
      throws java.lang.IndexOutOfBoundsException {
    this._seqRef.addElement(vSeqRef);
  }

  /**
   * 
   * 
   * @param index
   * @param vSeqRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addSeqRef(final int index, final java.lang.Object vSeqRef)
      throws java.lang.IndexOutOfBoundsException {
    this._seqRef.add(index, vSeqRef);
  }

  /**
   * Method enumerateSeqRef.
   * 
   * @return an Enumeration over all java.lang.Object elements
   */
  public java.util.Enumeration enumerateSeqRef() {
    return this._seqRef.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof DataSetAnnotations) {

      DataSetAnnotations temp = (DataSetAnnotations) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._seqRef != null) {
        if (temp._seqRef == null)
          return false;
        if (this._seqRef != temp._seqRef) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._seqRef);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._seqRef);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._seqRef);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqRef);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._seqRef.equals(temp._seqRef)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._seqRef);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqRef);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._seqRef);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._seqRef);
          }
        }
      } else if (temp._seqRef != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Method getSeqRef.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the java.lang.Object at the given index
   */
  public java.lang.Object getSeqRef(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._seqRef.size()) {
      throw new IndexOutOfBoundsException("getSeqRef: Index value '" + index
          + "' not in range [0.." + (this._seqRef.size() - 1) + "]");
    }

    return _seqRef.get(index);
  }

  /**
   * Method getSeqRef.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public java.lang.Object[] getSeqRef() {
    java.lang.Object[] array = new java.lang.Object[0];
    return (java.lang.Object[]) this._seqRef.toArray(array);
  }

  /**
   * Method getSeqRefAsReference.Returns a reference to '_seqRef'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getSeqRefAsReference() {
    return this._seqRef;
  }

  /**
   * Method getSeqRefCount.
   * 
   * @return the size of this collection
   */
  public int getSeqRefCount() {
    return this._seqRef.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_seqRef != null
        && !org.castor.util.CycleBreaker.startingToCycle(_seqRef)) {
      result = 37 * result + _seqRef.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_seqRef);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllSeqRef() {
    this._seqRef.clear();
  }

  /**
   * Method removeSeqRef.
   * 
   * @param vSeqRef
   * @return true if the object was removed from the collection.
   */
  public boolean removeSeqRef(final java.lang.Object vSeqRef) {
    boolean removed = _seqRef.remove(vSeqRef);
    return removed;
  }

  /**
   * Method removeSeqRefAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public java.lang.Object removeSeqRefAt(final int index) {
    java.lang.Object obj = this._seqRef.remove(index);
    return obj;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * 
   * 
   * @param index
   * @param vSeqRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setSeqRef(final int index, final java.lang.Object vSeqRef)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._seqRef.size()) {
      throw new IndexOutOfBoundsException("setSeqRef: Index value '" + index
          + "' not in range [0.." + (this._seqRef.size() - 1) + "]");
    }

    this._seqRef.set(index, vSeqRef);
  }

  /**
   * 
   * 
   * @param vSeqRefArray
   */
  public void setSeqRef(final java.lang.Object[] vSeqRefArray) {
    // -- copy array
    _seqRef.clear();

    for (int i = 0; i < vSeqRefArray.length; i++) {
      this._seqRef.add(vSeqRefArray[i]);
    }
  }

  /**
   * Sets the value of '_seqRef' by copying the given Vector. All elements will
   * be checked for type safety.
   * 
   * @param vSeqRefList
   *          the Vector to copy.
   */
  public void setSeqRef(final java.util.Vector vSeqRefList) {
    // copy vector
    this._seqRef.clear();

    this._seqRef.addAll(vSeqRefList);
  }

  /**
   * Sets the value of '_seqRef' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param seqRefVector
   *          the Vector to set.
   */
  public void setSeqRefAsReference(final java.util.Vector seqRefVector) {
    this._seqRef = seqRefVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.RangeType
   */
  public static uk.ac.vamsas.objects.core.RangeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.RangeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.DataSetAnnotations.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
