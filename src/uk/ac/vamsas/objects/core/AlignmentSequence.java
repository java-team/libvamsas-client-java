/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class AlignmentSequence.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class AlignmentSequence extends uk.ac.vamsas.objects.core.SequenceType
    implements java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Dataset Sequence from which this alignment sequence is taken from
   * 
   */
  private java.lang.Object _refid;

  /**
   * Field _alignmentSequenceAnnotationList.
   */
  private java.util.Vector _alignmentSequenceAnnotationList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public AlignmentSequence() {
    super();
    this._alignmentSequenceAnnotationList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vAlignmentSequenceAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentSequenceAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation vAlignmentSequenceAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentSequenceAnnotationList
        .addElement(vAlignmentSequenceAnnotation);
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentSequenceAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAlignmentSequenceAnnotation(
      final int index,
      final uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation vAlignmentSequenceAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    this._alignmentSequenceAnnotationList.add(index,
        vAlignmentSequenceAnnotation);
  }

  /**
   * Method enumerateAlignmentSequenceAnnotation.
   * 
   * @return an Enumeration over all
   *         uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation element
   */
  public java.util.Enumeration enumerateAlignmentSequenceAnnotation() {
    return this._alignmentSequenceAnnotationList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof AlignmentSequence) {

      AlignmentSequence temp = (AlignmentSequence) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._refid != null) {
        if (temp._refid == null)
          return false;
        if (this._refid != temp._refid) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._refid);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._refid);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._refid);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._refid);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._refid.equals(temp._refid)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._refid);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._refid);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._refid);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._refid);
          }
        }
      } else if (temp._refid != null)
        return false;
      if (this._alignmentSequenceAnnotationList != null) {
        if (temp._alignmentSequenceAnnotationList == null)
          return false;
        if (this._alignmentSequenceAnnotationList != temp._alignmentSequenceAnnotationList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._alignmentSequenceAnnotationList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._alignmentSequenceAnnotationList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentSequenceAnnotationList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentSequenceAnnotationList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._alignmentSequenceAnnotationList
                .equals(temp._alignmentSequenceAnnotationList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._alignmentSequenceAnnotationList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._alignmentSequenceAnnotationList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._alignmentSequenceAnnotationList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._alignmentSequenceAnnotationList);
          }
        }
      } else if (temp._alignmentSequenceAnnotationList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getAlignmentSequenceAnnotation.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the
   *         uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation getAlignmentSequenceAnnotation(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentSequenceAnnotationList.size()) {
      throw new IndexOutOfBoundsException(
          "getAlignmentSequenceAnnotation: Index value '" + index
              + "' not in range [0.."
              + (this._alignmentSequenceAnnotationList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation) _alignmentSequenceAnnotationList
        .get(index);
  }

  /**
   * Method getAlignmentSequenceAnnotation.Returns the contents of the
   * collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation[] getAlignmentSequenceAnnotation() {
    uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation[] array = new uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation[0];
    return (uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation[]) this._alignmentSequenceAnnotationList
        .toArray(array);
  }

  /**
   * Method getAlignmentSequenceAnnotationAsReference.Returns a reference to
   * '_alignmentSequenceAnnotationList'. No type checking is performed on any
   * modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAlignmentSequenceAnnotationAsReference() {
    return this._alignmentSequenceAnnotationList;
  }

  /**
   * Method getAlignmentSequenceAnnotationCount.
   * 
   * @return the size of this collection
   */
  public int getAlignmentSequenceAnnotationCount() {
    return this._alignmentSequenceAnnotationList.size();
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'refid'. The field 'refid' has the following
   * description: Dataset Sequence from which this alignment sequence is taken
   * from
   * 
   * 
   * @return the value of field 'Refid'.
   */
  public java.lang.Object getRefid() {
    return this._refid;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_refid != null && !org.castor.util.CycleBreaker.startingToCycle(_refid)) {
      result = 37 * result + _refid.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_refid);
    }
    if (_alignmentSequenceAnnotationList != null
        && !org.castor.util.CycleBreaker
            .startingToCycle(_alignmentSequenceAnnotationList)) {
      result = 37 * result + _alignmentSequenceAnnotationList.hashCode();
      org.castor.util.CycleBreaker
          .releaseCycleHandle(_alignmentSequenceAnnotationList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Method removeAlignmentSequenceAnnotation.
   * 
   * @param vAlignmentSequenceAnnotation
   * @return true if the object was removed from the collection.
   */
  public boolean removeAlignmentSequenceAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation vAlignmentSequenceAnnotation) {
    boolean removed = _alignmentSequenceAnnotationList
        .remove(vAlignmentSequenceAnnotation);
    return removed;
  }

  /**
   * Method removeAlignmentSequenceAnnotationAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation removeAlignmentSequenceAnnotationAt(
      final int index) {
    java.lang.Object obj = this._alignmentSequenceAnnotationList.remove(index);
    return (uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation) obj;
  }

  /**
     */
  public void removeAllAlignmentSequenceAnnotation() {
    this._alignmentSequenceAnnotationList.clear();
  }

  /**
   * 
   * 
   * @param index
   * @param vAlignmentSequenceAnnotation
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAlignmentSequenceAnnotation(
      final int index,
      final uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation vAlignmentSequenceAnnotation)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._alignmentSequenceAnnotationList.size()) {
      throw new IndexOutOfBoundsException(
          "setAlignmentSequenceAnnotation: Index value '" + index
              + "' not in range [0.."
              + (this._alignmentSequenceAnnotationList.size() - 1) + "]");
    }

    this._alignmentSequenceAnnotationList.set(index,
        vAlignmentSequenceAnnotation);
  }

  /**
   * 
   * 
   * @param vAlignmentSequenceAnnotationArray
   */
  public void setAlignmentSequenceAnnotation(
      final uk.ac.vamsas.objects.core.AlignmentSequenceAnnotation[] vAlignmentSequenceAnnotationArray) {
    // -- copy array
    _alignmentSequenceAnnotationList.clear();

    for (int i = 0; i < vAlignmentSequenceAnnotationArray.length; i++) {
      this._alignmentSequenceAnnotationList
          .add(vAlignmentSequenceAnnotationArray[i]);
    }
  }

  /**
   * Sets the value of '_alignmentSequenceAnnotationList' by copying the given
   * Vector. All elements will be checked for type safety.
   * 
   * @param vAlignmentSequenceAnnotationList
   *          the Vector to copy.
   */
  public void setAlignmentSequenceAnnotation(
      final java.util.Vector vAlignmentSequenceAnnotationList) {
    // copy vector
    this._alignmentSequenceAnnotationList.clear();

    this._alignmentSequenceAnnotationList
        .addAll(vAlignmentSequenceAnnotationList);
  }

  /**
   * Sets the value of '_alignmentSequenceAnnotationList' by setting it to the
   * given Vector. No type checking is performed.
   * 
   * @deprecated
   * 
   * @param alignmentSequenceAnnotationVector
   *          the Vector to set.
   */
  public void setAlignmentSequenceAnnotationAsReference(
      final java.util.Vector alignmentSequenceAnnotationVector) {
    this._alignmentSequenceAnnotationList = alignmentSequenceAnnotationVector;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'refid'. The field 'refid' has the following
   * description: Dataset Sequence from which this alignment sequence is taken
   * from
   * 
   * 
   * @param refid
   *          the value of field 'refid'.
   */
  public void setRefid(final java.lang.Object refid) {
    this._refid = refid;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.SequenceTyp
   */
  public static uk.ac.vamsas.objects.core.SequenceType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.SequenceType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.AlignmentSequence.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
