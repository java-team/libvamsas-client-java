/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Sequence.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Sequence extends uk.ac.vamsas.objects.core.SequenceType implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * symbol class for sequence
   * 
   */
  private java.lang.String _dictionary;

  /**
   * Store a list of database references for this sequence record - with
   * optional mapping from database sequence to the given sequence record
   */
  private java.util.Vector _dbRefList;

  /**
   * explicitly named cross reference to other objects in the document.
   */
  private java.util.Vector _vxrefList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Sequence() {
    super();
    this._dbRefList = new java.util.Vector();
    this._vxrefList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vDbRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDbRef(final uk.ac.vamsas.objects.core.DbRef vDbRef)
      throws java.lang.IndexOutOfBoundsException {
    this._dbRefList.addElement(vDbRef);
  }

  /**
   * 
   * 
   * @param index
   * @param vDbRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addDbRef(final int index,
      final uk.ac.vamsas.objects.core.DbRef vDbRef)
      throws java.lang.IndexOutOfBoundsException {
    this._dbRefList.add(index, vDbRef);
  }

  /**
   * 
   * 
   * @param vVxref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVxref(final uk.ac.vamsas.objects.core.Vxref vVxref)
      throws java.lang.IndexOutOfBoundsException {
    this._vxrefList.addElement(vVxref);
  }

  /**
   * 
   * 
   * @param index
   * @param vVxref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addVxref(final int index,
      final uk.ac.vamsas.objects.core.Vxref vVxref)
      throws java.lang.IndexOutOfBoundsException {
    this._vxrefList.add(index, vVxref);
  }

  /**
   * Method enumerateDbRef.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.DbRef elements
   */
  public java.util.Enumeration enumerateDbRef() {
    return this._dbRefList.elements();
  }

  /**
   * Method enumerateVxref.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Vxref elements
   */
  public java.util.Enumeration enumerateVxref() {
    return this._vxrefList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Sequence) {

      Sequence temp = (Sequence) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._dictionary != null) {
        if (temp._dictionary == null)
          return false;
        if (this._dictionary != temp._dictionary) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._dictionary);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._dictionary);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dictionary);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dictionary);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dictionary.equals(temp._dictionary)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dictionary);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dictionary);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._dictionary);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._dictionary);
          }
        }
      } else if (temp._dictionary != null)
        return false;
      if (this._dbRefList != null) {
        if (temp._dbRefList == null)
          return false;
        if (this._dbRefList != temp._dbRefList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._dbRefList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._dbRefList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dbRefList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dbRefList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._dbRefList.equals(temp._dbRefList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._dbRefList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._dbRefList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._dbRefList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._dbRefList);
          }
        }
      } else if (temp._dbRefList != null)
        return false;
      if (this._vxrefList != null) {
        if (temp._vxrefList == null)
          return false;
        if (this._vxrefList != temp._vxrefList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._vxrefList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._vxrefList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._vxrefList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._vxrefList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._vxrefList.equals(temp._vxrefList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._vxrefList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._vxrefList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._vxrefList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._vxrefList);
          }
        }
      } else if (temp._vxrefList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getDbRef.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.DbRef at the given index
   */
  public uk.ac.vamsas.objects.core.DbRef getDbRef(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dbRefList.size()) {
      throw new IndexOutOfBoundsException("getDbRef: Index value '" + index
          + "' not in range [0.." + (this._dbRefList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.DbRef) _dbRefList.get(index);
  }

  /**
   * Method getDbRef.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.DbRef[] getDbRef() {
    uk.ac.vamsas.objects.core.DbRef[] array = new uk.ac.vamsas.objects.core.DbRef[0];
    return (uk.ac.vamsas.objects.core.DbRef[]) this._dbRefList.toArray(array);
  }

  /**
   * Method getDbRefAsReference.Returns a reference to '_dbRefList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getDbRefAsReference() {
    return this._dbRefList;
  }

  /**
   * Method getDbRefCount.
   * 
   * @return the size of this collection
   */
  public int getDbRefCount() {
    return this._dbRefList.size();
  }

  /**
   * Returns the value of field 'dictionary'. The field 'dictionary' has the
   * following description: symbol class for sequence
   * 
   * 
   * @return the value of field 'Dictionary'.
   */
  public java.lang.String getDictionary() {
    return this._dictionary;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Method getVxref.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Vxref at the given index
   */
  public uk.ac.vamsas.objects.core.Vxref getVxref(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._vxrefList.size()) {
      throw new IndexOutOfBoundsException("getVxref: Index value '" + index
          + "' not in range [0.." + (this._vxrefList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Vxref) _vxrefList.get(index);
  }

  /**
   * Method getVxref.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Vxref[] getVxref() {
    uk.ac.vamsas.objects.core.Vxref[] array = new uk.ac.vamsas.objects.core.Vxref[0];
    return (uk.ac.vamsas.objects.core.Vxref[]) this._vxrefList.toArray(array);
  }

  /**
   * Method getVxrefAsReference.Returns a reference to '_vxrefList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getVxrefAsReference() {
    return this._vxrefList;
  }

  /**
   * Method getVxrefCount.
   * 
   * @return the size of this collection
   */
  public int getVxrefCount() {
    return this._vxrefList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_dictionary != null
        && !org.castor.util.CycleBreaker.startingToCycle(_dictionary)) {
      result = 37 * result + _dictionary.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dictionary);
    }
    if (_dbRefList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_dbRefList)) {
      result = 37 * result + _dbRefList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_dbRefList);
    }
    if (_vxrefList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_vxrefList)) {
      result = 37 * result + _vxrefList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_vxrefList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllDbRef() {
    this._dbRefList.clear();
  }

  /**
     */
  public void removeAllVxref() {
    this._vxrefList.clear();
  }

  /**
   * Method removeDbRef.
   * 
   * @param vDbRef
   * @return true if the object was removed from the collection.
   */
  public boolean removeDbRef(final uk.ac.vamsas.objects.core.DbRef vDbRef) {
    boolean removed = _dbRefList.remove(vDbRef);
    return removed;
  }

  /**
   * Method removeDbRefAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.DbRef removeDbRefAt(final int index) {
    java.lang.Object obj = this._dbRefList.remove(index);
    return (uk.ac.vamsas.objects.core.DbRef) obj;
  }

  /**
   * Method removeVxref.
   * 
   * @param vVxref
   * @return true if the object was removed from the collection.
   */
  public boolean removeVxref(final uk.ac.vamsas.objects.core.Vxref vVxref) {
    boolean removed = _vxrefList.remove(vVxref);
    return removed;
  }

  /**
   * Method removeVxrefAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Vxref removeVxrefAt(final int index) {
    java.lang.Object obj = this._vxrefList.remove(index);
    return (uk.ac.vamsas.objects.core.Vxref) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vDbRef
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setDbRef(final int index,
      final uk.ac.vamsas.objects.core.DbRef vDbRef)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._dbRefList.size()) {
      throw new IndexOutOfBoundsException("setDbRef: Index value '" + index
          + "' not in range [0.." + (this._dbRefList.size() - 1) + "]");
    }

    this._dbRefList.set(index, vDbRef);
  }

  /**
   * 
   * 
   * @param vDbRefArray
   */
  public void setDbRef(final uk.ac.vamsas.objects.core.DbRef[] vDbRefArray) {
    // -- copy array
    _dbRefList.clear();

    for (int i = 0; i < vDbRefArray.length; i++) {
      this._dbRefList.add(vDbRefArray[i]);
    }
  }

  /**
   * Sets the value of '_dbRefList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vDbRefList
   *          the Vector to copy.
   */
  public void setDbRef(final java.util.Vector vDbRefList) {
    // copy vector
    this._dbRefList.clear();

    this._dbRefList.addAll(vDbRefList);
  }

  /**
   * Sets the value of '_dbRefList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param dbRefVector
   *          the Vector to set.
   */
  public void setDbRefAsReference(final java.util.Vector dbRefVector) {
    this._dbRefList = dbRefVector;
  }

  /**
   * Sets the value of field 'dictionary'. The field 'dictionary' has the
   * following description: symbol class for sequence
   * 
   * 
   * @param dictionary
   *          the value of field 'dictionary'.
   */
  public void setDictionary(final java.lang.String dictionary) {
    this._dictionary = dictionary;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * 
   * 
   * @param index
   * @param vVxref
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setVxref(final int index,
      final uk.ac.vamsas.objects.core.Vxref vVxref)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._vxrefList.size()) {
      throw new IndexOutOfBoundsException("setVxref: Index value '" + index
          + "' not in range [0.." + (this._vxrefList.size() - 1) + "]");
    }

    this._vxrefList.set(index, vVxref);
  }

  /**
   * 
   * 
   * @param vVxrefArray
   */
  public void setVxref(final uk.ac.vamsas.objects.core.Vxref[] vVxrefArray) {
    // -- copy array
    _vxrefList.clear();

    for (int i = 0; i < vVxrefArray.length; i++) {
      this._vxrefList.add(vVxrefArray[i]);
    }
  }

  /**
   * Sets the value of '_vxrefList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vVxrefList
   *          the Vector to copy.
   */
  public void setVxref(final java.util.Vector vVxrefList) {
    // copy vector
    this._vxrefList.clear();

    this._vxrefList.addAll(vVxrefList);
  }

  /**
   * Sets the value of '_vxrefList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param vxrefVector
   *          the Vector to set.
   */
  public void setVxrefAsReference(final java.util.Vector vxrefVector) {
    this._vxrefList = vxrefVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.SequenceTyp
   */
  public static uk.ac.vamsas.objects.core.SequenceType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.SequenceType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Sequence.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
