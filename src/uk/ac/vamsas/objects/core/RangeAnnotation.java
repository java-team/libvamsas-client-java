/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Annotation for a rangeSpec - values can be attached for the whole
 * specification, and to each position within the spec. following the
 * orientation specified by the ordered set of rangeSpec (pos, seg) elements.
 * 
 * @version $Revision$ $Date$
 */
public class RangeAnnotation extends uk.ac.vamsas.objects.core.RangeType
    implements java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  /**
   * Annotation with the same non-empty group name are grouped together
   */
  private java.lang.String _group = "";

  /**
   * A Das Feature has both a type and a Type ID. We go the route of requiring
   * the type string to be taken from a controlled vocabulary if an application
   * expects others to make sense of it. The type may qualified - so
   * uniprot:CHAIN is a valid type name, and considered distinct from
   * someotherDB:CHAIN
   */
  private java.lang.String _type;

  /**
   * Short, meaningful name for the annotation - if this is absent, then the
   * type string should be used in its place.
   * 
   */
  private java.lang.String _label;

  /**
   * Human readable description of the annotation
   * 
   */
  private java.lang.String _description;

  /**
   * TODO: specify this - we have considered taking the GO evidence codes as a
   * model for assessing a measure of quality to an annotation.
   */
  private java.lang.String _status;

  /**
   * Annotation Element position maps to ordered positions defined by the
   * sequence of rangeType pos positions or concatenated seg start/end segments.
   */
  private java.util.Vector _annotationElementList;

  /**
   * Ordered set of optionally named float values for the whole annotation
   */
  private java.util.Vector _scoreList;

  /**
   * Field _linkList.
   */
  private java.util.Vector _linkList;

  /**
   * Note:These are mutable so an application should check them each time.
   */
  private java.util.Vector _propertyList;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public RangeAnnotation() {
    super();
    setGroup("");
    this._annotationElementList = new java.util.Vector();
    this._scoreList = new java.util.Vector();
    this._linkList = new java.util.Vector();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vAnnotationElement
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAnnotationElement(
      final uk.ac.vamsas.objects.core.AnnotationElement vAnnotationElement)
      throws java.lang.IndexOutOfBoundsException {
    this._annotationElementList.addElement(vAnnotationElement);
  }

  /**
   * 
   * 
   * @param index
   * @param vAnnotationElement
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addAnnotationElement(final int index,
      final uk.ac.vamsas.objects.core.AnnotationElement vAnnotationElement)
      throws java.lang.IndexOutOfBoundsException {
    this._annotationElementList.add(index, vAnnotationElement);
  }

  /**
   * 
   * 
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addLink(final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    this._linkList.addElement(vLink);
  }

  /**
   * 
   * 
   * @param index
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addLink(final int index,
      final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    this._linkList.add(index, vLink);
  }

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * 
   * 
   * @param vScore
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addScore(final uk.ac.vamsas.objects.core.Score vScore)
      throws java.lang.IndexOutOfBoundsException {
    this._scoreList.addElement(vScore);
  }

  /**
   * 
   * 
   * @param index
   * @param vScore
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addScore(final int index,
      final uk.ac.vamsas.objects.core.Score vScore)
      throws java.lang.IndexOutOfBoundsException {
    this._scoreList.add(index, vScore);
  }

  /**
   * Method enumerateAnnotationElement.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.AnnotationElement
   *         elements
   */
  public java.util.Enumeration enumerateAnnotationElement() {
    return this._annotationElementList.elements();
  }

  /**
   * Method enumerateLink.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Link elements
   */
  public java.util.Enumeration enumerateLink() {
    return this._linkList.elements();
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Method enumerateScore.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Score elements
   */
  public java.util.Enumeration enumerateScore() {
    return this._scoreList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof RangeAnnotation) {

      RangeAnnotation temp = (RangeAnnotation) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      if (this._group != null) {
        if (temp._group == null)
          return false;
        if (this._group != temp._group) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._group);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._group);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._group);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._group);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._group.equals(temp._group)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._group);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._group);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._group);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._group);
          }
        }
      } else if (temp._group != null)
        return false;
      if (this._type != null) {
        if (temp._type == null)
          return false;
        if (this._type != temp._type) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._type);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._type);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._type.equals(temp._type)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._type);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._type);
          }
        }
      } else if (temp._type != null)
        return false;
      if (this._label != null) {
        if (temp._label == null)
          return false;
        if (this._label != temp._label) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._label);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._label);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._label);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._label);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._label.equals(temp._label)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._label);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._label);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._label);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._label);
          }
        }
      } else if (temp._label != null)
        return false;
      if (this._description != null) {
        if (temp._description == null)
          return false;
        if (this._description != temp._description) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._description);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._description);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._description.equals(temp._description)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._description);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._description);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._description);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._description);
          }
        }
      } else if (temp._description != null)
        return false;
      if (this._status != null) {
        if (temp._status == null)
          return false;
        if (this._status != temp._status) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._status);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._status);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._status);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._status);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._status.equals(temp._status)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._status);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._status);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._status);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._status);
          }
        }
      } else if (temp._status != null)
        return false;
      if (this._annotationElementList != null) {
        if (temp._annotationElementList == null)
          return false;
        if (this._annotationElementList != temp._annotationElementList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._annotationElementList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._annotationElementList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._annotationElementList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._annotationElementList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._annotationElementList
                .equals(temp._annotationElementList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._annotationElementList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._annotationElementList);
              return false;
            }
            org.castor.util.CycleBreaker
                .releaseCycleHandle(this._annotationElementList);
            org.castor.util.CycleBreaker
                .releaseCycleHandle(temp._annotationElementList);
          }
        }
      } else if (temp._annotationElementList != null)
        return false;
      if (this._scoreList != null) {
        if (temp._scoreList == null)
          return false;
        if (this._scoreList != temp._scoreList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._scoreList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._scoreList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._scoreList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._scoreList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._scoreList.equals(temp._scoreList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._scoreList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._scoreList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._scoreList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._scoreList);
          }
        }
      } else if (temp._scoreList != null)
        return false;
      if (this._linkList != null) {
        if (temp._linkList == null)
          return false;
        if (this._linkList != temp._linkList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._linkList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._linkList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._linkList.equals(temp._linkList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._linkList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._linkList);
          }
        }
      } else if (temp._linkList != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Method getAnnotationElement.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.AnnotationElement at the
   *         given inde
   */
  public uk.ac.vamsas.objects.core.AnnotationElement getAnnotationElement(
      final int index) throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._annotationElementList.size()) {
      throw new IndexOutOfBoundsException("getAnnotationElement: Index value '"
          + index + "' not in range [0.."
          + (this._annotationElementList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.AnnotationElement) _annotationElementList
        .get(index);
  }

  /**
   * Method getAnnotationElement.Returns the contents of the collection in an
   * Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.AnnotationElement[] getAnnotationElement() {
    uk.ac.vamsas.objects.core.AnnotationElement[] array = new uk.ac.vamsas.objects.core.AnnotationElement[0];
    return (uk.ac.vamsas.objects.core.AnnotationElement[]) this._annotationElementList
        .toArray(array);
  }

  /**
   * Method getAnnotationElementAsReference.Returns a reference to
   * '_annotationElementList'. No type checking is performed on any
   * modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getAnnotationElementAsReference() {
    return this._annotationElementList;
  }

  /**
   * Method getAnnotationElementCount.
   * 
   * @return the size of this collection
   */
  public int getAnnotationElementCount() {
    return this._annotationElementList.size();
  }

  /**
   * Returns the value of field 'description'. The field 'description' has the
   * following description: Human readable description of the annotation
   * 
   * 
   * @return the value of field 'Description'.
   */
  public java.lang.String getDescription() {
    return this._description;
  }

  /**
   * Returns the value of field 'group'. The field 'group' has the following
   * description: Annotation with the same non-empty group name are grouped
   * together
   * 
   * @return the value of field 'Group'.
   */
  public java.lang.String getGroup() {
    return this._group;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'label'. The field 'label' has the following
   * description: Short, meaningful name for the annotation - if this is absent,
   * then the type string should be used in its place.
   * 
   * 
   * @return the value of field 'Label'.
   */
  public java.lang.String getLabel() {
    return this._label;
  }

  /**
   * Method getLink.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Link at the given index
   */
  public uk.ac.vamsas.objects.core.Link getLink(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._linkList.size()) {
      throw new IndexOutOfBoundsException("getLink: Index value '" + index
          + "' not in range [0.." + (this._linkList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Link) _linkList.get(index);
  }

  /**
   * Method getLink.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Link[] getLink() {
    uk.ac.vamsas.objects.core.Link[] array = new uk.ac.vamsas.objects.core.Link[0];
    return (uk.ac.vamsas.objects.core.Link[]) this._linkList.toArray(array);
  }

  /**
   * Method getLinkAsReference.Returns a reference to '_linkList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getLinkAsReference() {
    return this._linkList;
  }

  /**
   * Method getLinkCount.
   * 
   * @return the size of this collection
   */
  public int getLinkCount() {
    return this._linkList.size();
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Method getScore.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Score at the given index
   */
  public uk.ac.vamsas.objects.core.Score getScore(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._scoreList.size()) {
      throw new IndexOutOfBoundsException("getScore: Index value '" + index
          + "' not in range [0.." + (this._scoreList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Score) _scoreList.get(index);
  }

  /**
   * Method getScore.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Score[] getScore() {
    uk.ac.vamsas.objects.core.Score[] array = new uk.ac.vamsas.objects.core.Score[0];
    return (uk.ac.vamsas.objects.core.Score[]) this._scoreList.toArray(array);
  }

  /**
   * Method getScoreAsReference.Returns a reference to '_scoreList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getScoreAsReference() {
    return this._scoreList;
  }

  /**
   * Method getScoreCount.
   * 
   * @return the size of this collection
   */
  public int getScoreCount() {
    return this._scoreList.size();
  }

  /**
   * Returns the value of field 'status'. The field 'status' has the following
   * description: TODO: specify this - we have considered taking the GO evidence
   * codes as a model for assessing a measure of quality to an annotation.
   * 
   * @return the value of field 'Status'.
   */
  public java.lang.String getStatus() {
    return this._status;
  }

  /**
   * Returns the value of field 'type'. The field 'type' has the following
   * description: A Das Feature has both a type and a Type ID. We go the route
   * of requiring the type string to be taken from a controlled vocabulary if an
   * application expects others to make sense of it. The type may qualified - so
   * uniprot:CHAIN is a valid type name, and considered distinct from
   * someotherDB:CHAIN
   * 
   * @return the value of field 'Type'.
   */
  public java.lang.String getType() {
    return this._type;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }
    if (_group != null && !org.castor.util.CycleBreaker.startingToCycle(_group)) {
      result = 37 * result + _group.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_group);
    }
    if (_type != null && !org.castor.util.CycleBreaker.startingToCycle(_type)) {
      result = 37 * result + _type.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_type);
    }
    if (_label != null && !org.castor.util.CycleBreaker.startingToCycle(_label)) {
      result = 37 * result + _label.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_label);
    }
    if (_description != null
        && !org.castor.util.CycleBreaker.startingToCycle(_description)) {
      result = 37 * result + _description.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_description);
    }
    if (_status != null
        && !org.castor.util.CycleBreaker.startingToCycle(_status)) {
      result = 37 * result + _status.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_status);
    }
    if (_annotationElementList != null
        && !org.castor.util.CycleBreaker
            .startingToCycle(_annotationElementList)) {
      result = 37 * result + _annotationElementList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_annotationElementList);
    }
    if (_scoreList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_scoreList)) {
      result = 37 * result + _scoreList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_scoreList);
    }
    if (_linkList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_linkList)) {
      result = 37 * result + _linkList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_linkList);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllAnnotationElement() {
    this._annotationElementList.clear();
  }

  /**
     */
  public void removeAllLink() {
    this._linkList.clear();
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
     */
  public void removeAllScore() {
    this._scoreList.clear();
  }

  /**
   * Method removeAnnotationElement.
   * 
   * @param vAnnotationElement
   * @return true if the object was removed from the collection.
   */
  public boolean removeAnnotationElement(
      final uk.ac.vamsas.objects.core.AnnotationElement vAnnotationElement) {
    boolean removed = _annotationElementList.remove(vAnnotationElement);
    return removed;
  }

  /**
   * Method removeAnnotationElementAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.AnnotationElement removeAnnotationElementAt(
      final int index) {
    java.lang.Object obj = this._annotationElementList.remove(index);
    return (uk.ac.vamsas.objects.core.AnnotationElement) obj;
  }

  /**
   * Method removeLink.
   * 
   * @param vLink
   * @return true if the object was removed from the collection.
   */
  public boolean removeLink(final uk.ac.vamsas.objects.core.Link vLink) {
    boolean removed = _linkList.remove(vLink);
    return removed;
  }

  /**
   * Method removeLinkAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Link removeLinkAt(final int index) {
    java.lang.Object obj = this._linkList.remove(index);
    return (uk.ac.vamsas.objects.core.Link) obj;
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Method removeScore.
   * 
   * @param vScore
   * @return true if the object was removed from the collection.
   */
  public boolean removeScore(final uk.ac.vamsas.objects.core.Score vScore) {
    boolean removed = _scoreList.remove(vScore);
    return removed;
  }

  /**
   * Method removeScoreAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Score removeScoreAt(final int index) {
    java.lang.Object obj = this._scoreList.remove(index);
    return (uk.ac.vamsas.objects.core.Score) obj;
  }

  /**
   * 
   * 
   * @param index
   * @param vAnnotationElement
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setAnnotationElement(final int index,
      final uk.ac.vamsas.objects.core.AnnotationElement vAnnotationElement)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._annotationElementList.size()) {
      throw new IndexOutOfBoundsException("setAnnotationElement: Index value '"
          + index + "' not in range [0.."
          + (this._annotationElementList.size() - 1) + "]");
    }

    this._annotationElementList.set(index, vAnnotationElement);
  }

  /**
   * 
   * 
   * @param vAnnotationElementArray
   */
  public void setAnnotationElement(
      final uk.ac.vamsas.objects.core.AnnotationElement[] vAnnotationElementArray) {
    // -- copy array
    _annotationElementList.clear();

    for (int i = 0; i < vAnnotationElementArray.length; i++) {
      this._annotationElementList.add(vAnnotationElementArray[i]);
    }
  }

  /**
   * Sets the value of '_annotationElementList' by copying the given Vector. All
   * elements will be checked for type safety.
   * 
   * @param vAnnotationElementList
   *          the Vector to copy.
   */
  public void setAnnotationElement(final java.util.Vector vAnnotationElementList) {
    // copy vector
    this._annotationElementList.clear();

    this._annotationElementList.addAll(vAnnotationElementList);
  }

  /**
   * Sets the value of '_annotationElementList' by setting it to the given
   * Vector. No type checking is performed.
   * 
   * @deprecated
   * 
   * @param annotationElementVector
   *          the Vector to set.
   */
  public void setAnnotationElementAsReference(
      final java.util.Vector annotationElementVector) {
    this._annotationElementList = annotationElementVector;
  }

  /**
   * Sets the value of field 'description'. The field 'description' has the
   * following description: Human readable description of the annotation
   * 
   * 
   * @param description
   *          the value of field 'description'.
   */
  public void setDescription(final java.lang.String description) {
    this._description = description;
  }

  /**
   * Sets the value of field 'group'. The field 'group' has the following
   * description: Annotation with the same non-empty group name are grouped
   * together
   * 
   * @param group
   *          the value of field 'group'.
   */
  public void setGroup(final java.lang.String group) {
    this._group = group;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'label'. The field 'label' has the following
   * description: Short, meaningful name for the annotation - if this is absent,
   * then the type string should be used in its place.
   * 
   * 
   * @param label
   *          the value of field 'label'.
   */
  public void setLabel(final java.lang.String label) {
    this._label = label;
  }

  /**
   * 
   * 
   * @param index
   * @param vLink
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setLink(final int index,
      final uk.ac.vamsas.objects.core.Link vLink)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._linkList.size()) {
      throw new IndexOutOfBoundsException("setLink: Index value '" + index
          + "' not in range [0.." + (this._linkList.size() - 1) + "]");
    }

    this._linkList.set(index, vLink);
  }

  /**
   * 
   * 
   * @param vLinkArray
   */
  public void setLink(final uk.ac.vamsas.objects.core.Link[] vLinkArray) {
    // -- copy array
    _linkList.clear();

    for (int i = 0; i < vLinkArray.length; i++) {
      this._linkList.add(vLinkArray[i]);
    }
  }

  /**
   * Sets the value of '_linkList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vLinkList
   *          the Vector to copy.
   */
  public void setLink(final java.util.Vector vLinkList) {
    // copy vector
    this._linkList.clear();

    this._linkList.addAll(vLinkList);
  }

  /**
   * Sets the value of '_linkList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param linkVector
   *          the Vector to set.
   */
  public void setLinkAsReference(final java.util.Vector linkVector) {
    this._linkList = linkVector;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vScore
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setScore(final int index,
      final uk.ac.vamsas.objects.core.Score vScore)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._scoreList.size()) {
      throw new IndexOutOfBoundsException("setScore: Index value '" + index
          + "' not in range [0.." + (this._scoreList.size() - 1) + "]");
    }

    this._scoreList.set(index, vScore);
  }

  /**
   * 
   * 
   * @param vScoreArray
   */
  public void setScore(final uk.ac.vamsas.objects.core.Score[] vScoreArray) {
    // -- copy array
    _scoreList.clear();

    for (int i = 0; i < vScoreArray.length; i++) {
      this._scoreList.add(vScoreArray[i]);
    }
  }

  /**
   * Sets the value of '_scoreList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vScoreList
   *          the Vector to copy.
   */
  public void setScore(final java.util.Vector vScoreList) {
    // copy vector
    this._scoreList.clear();

    this._scoreList.addAll(vScoreList);
  }

  /**
   * Sets the value of '_scoreList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param scoreVector
   *          the Vector to set.
   */
  public void setScoreAsReference(final java.util.Vector scoreVector) {
    this._scoreList = scoreVector;
  }

  /**
   * Sets the value of field 'status'. The field 'status' has the following
   * description: TODO: specify this - we have considered taking the GO evidence
   * codes as a model for assessing a measure of quality to an annotation.
   * 
   * @param status
   *          the value of field 'status'.
   */
  public void setStatus(final java.lang.String status) {
    this._status = status;
  }

  /**
   * Sets the value of field 'type'. The field 'type' has the following
   * description: A Das Feature has both a type and a Type ID. We go the route
   * of requiring the type string to be taken from a controlled vocabulary if an
   * application expects others to make sense of it. The type may qualified - so
   * uniprot:CHAIN is a valid type name, and considered distinct from
   * someotherDB:CHAIN
   * 
   * @param type
   *          the value of field 'type'.
   */
  public void setType(final java.lang.String type) {
    this._type = type;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.RangeType
   */
  public static uk.ac.vamsas.objects.core.RangeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.RangeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.RangeAnnotation.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
