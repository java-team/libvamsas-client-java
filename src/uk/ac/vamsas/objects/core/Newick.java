/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Newick.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Newick extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * internal content storage
   */
  private java.lang.String _content = "";

  /**
   * Field _title.
   */
  private java.lang.String _title;

  /**
   * Primary Key for vamsas object referencing
   * 
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Newick() {
    super();
    setContent("");
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Newick) {

      Newick temp = (Newick) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._content != null) {
        if (temp._content == null)
          return false;
        if (this._content != temp._content) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._content);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._content);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._content.equals(temp._content)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._content);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._content);
          }
        }
      } else if (temp._content != null)
        return false;
      if (this._title != null) {
        if (temp._title == null)
          return false;
        if (this._title != temp._title) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._title);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._title);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._title.equals(temp._title)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
          }
        }
      } else if (temp._title != null)
        return false;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @return the value of field 'Content'.
   */
  public java.lang.String getContent() {
    return this._content;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Returns the value of field 'title'.
   * 
   * @return the value of field 'Title'.
   */
  public java.lang.String getTitle() {
    return this._title;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_content != null
        && !org.castor.util.CycleBreaker.startingToCycle(_content)) {
      result = 37 * result + _content.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_content);
    }
    if (_title != null && !org.castor.util.CycleBreaker.startingToCycle(_title)) {
      result = 37 * result + _title.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_title);
    }
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'content'. The field 'content' has the following
   * description: internal content storage
   * 
   * @param content
   *          the value of field 'content'.
   */
  public void setContent(final java.lang.String content) {
    this._content = content;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * Sets the value of field 'title'.
   * 
   * @param title
   *          the value of field 'title'.
   */
  public void setTitle(final java.lang.String title) {
    this._title = title;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Newick
   */
  public static uk.ac.vamsas.objects.core.Newick unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Newick) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Newick.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
