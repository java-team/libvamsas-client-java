/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * a region from start to end, with flag for inclusivity of terminii
 * 
 * @version $Revision$ $Date$
 */
public class Seg extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _start.
   */
  private int _start;

  /**
   * keeps track of state for field: _start
   */
  private boolean _has_start;

  /**
   * Field _end.
   */
  private int _end;

  /**
   * keeps track of state for field: _end
   */
  private boolean _has_end;

  /**
   * when false, a consecutive range like 'start=1, end=2' means the region
   * lying after position 1 and before position 2
   * 
   */
  private boolean _inclusive;

  /**
   * keeps track of state for field: _inclusive
   */
  private boolean _has_inclusive;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Seg() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
     */
  public void deleteEnd() {
    this._has_end = false;
  }

  /**
     */
  public void deleteInclusive() {
    this._has_inclusive = false;
  }

  /**
     */
  public void deleteStart() {
    this._has_start = false;
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Seg) {

      Seg temp = (Seg) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._start != temp._start)
        return false;
      if (this._has_start != temp._has_start)
        return false;
      if (this._end != temp._end)
        return false;
      if (this._has_end != temp._has_end)
        return false;
      if (this._inclusive != temp._inclusive)
        return false;
      if (this._has_inclusive != temp._has_inclusive)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'end'.
   * 
   * @return the value of field 'End'.
   */
  public int getEnd() {
    return this._end;
  }

  /**
   * Returns the value of field 'inclusive'. The field 'inclusive' has the
   * following description: when false, a consecutive range like 'start=1,
   * end=2' means the region lying after position 1 and before position 2
   * 
   * 
   * @return the value of field 'Inclusive'.
   */
  public boolean getInclusive() {
    return this._inclusive;
  }

  /**
   * Returns the value of field 'start'.
   * 
   * @return the value of field 'Start'.
   */
  public int getStart() {
    return this._start;
  }

  /**
   * Method hasEnd.
   * 
   * @return true if at least one End has been added
   */
  public boolean hasEnd() {
    return this._has_end;
  }

  /**
   * Method hasInclusive.
   * 
   * @return true if at least one Inclusive has been added
   */
  public boolean hasInclusive() {
    return this._has_inclusive;
  }

  /**
   * Method hasStart.
   * 
   * @return true if at least one Start has been added
   */
  public boolean hasStart() {
    return this._has_start;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + _start;
    result = 37 * result + _end;
    result = 37 * result + (_inclusive ? 0 : 1);

    return result;
  }

  /**
   * Returns the value of field 'inclusive'. The field 'inclusive' has the
   * following description: when false, a consecutive range like 'start=1,
   * end=2' means the region lying after position 1 and before position 2
   * 
   * 
   * @return the value of field 'Inclusive'.
   */
  public boolean isInclusive() {
    return this._inclusive;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'end'.
   * 
   * @param end
   *          the value of field 'end'.
   */
  public void setEnd(final int end) {
    this._end = end;
    this._has_end = true;
  }

  /**
   * Sets the value of field 'inclusive'. The field 'inclusive' has the
   * following description: when false, a consecutive range like 'start=1,
   * end=2' means the region lying after position 1 and before position 2
   * 
   * 
   * @param inclusive
   *          the value of field 'inclusive'.
   */
  public void setInclusive(final boolean inclusive) {
    this._inclusive = inclusive;
    this._has_inclusive = true;
  }

  /**
   * Sets the value of field 'start'.
   * 
   * @param start
   *          the value of field 'start'.
   */
  public void setStart(final int start) {
    this._start = start;
    this._has_start = true;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Seg
   */
  public static uk.ac.vamsas.objects.core.Seg unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Seg) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Seg.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
