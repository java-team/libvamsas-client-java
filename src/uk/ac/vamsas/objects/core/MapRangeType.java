/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class MapRangeType.
 * 
 * @version $Revision$ $Date$
 */
public class MapRangeType extends uk.ac.vamsas.objects.core.RangeType implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * number of dictionary symbol widths involved in each mapped position on this
   * sequence (for example, 3 for a dna sequence exon region that is being
   * mapped to a protein sequence). This is optional, since the unit can be
   * usually be inferred from the dictionary type of each sequence involved in
   * the mapping.
   */
  private long _unit;

  /**
   * keeps track of state for field: _unit
   */
  private boolean _has_unit;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public MapRangeType() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
     */
  public void deleteUnit() {
    this._has_unit = false;
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof MapRangeType) {

      MapRangeType temp = (MapRangeType) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._unit != temp._unit)
        return false;
      if (this._has_unit != temp._has_unit)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'unit'. The field 'unit' has the following
   * description: number of dictionary symbol widths involved in each mapped
   * position on this sequence (for example, 3 for a dna sequence exon region
   * that is being mapped to a protein sequence). This is optional, since the
   * unit can be usually be inferred from the dictionary type of each sequence
   * involved in the mapping.
   * 
   * @return the value of field 'Unit'.
   */
  public long getUnit() {
    return this._unit;
  }

  /**
   * Method hasUnit.
   * 
   * @return true if at least one Unit has been added
   */
  public boolean hasUnit() {
    return this._has_unit;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    result = 37 * result + (int) (_unit ^ (_unit >>> 32));

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'unit'. The field 'unit' has the following
   * description: number of dictionary symbol widths involved in each mapped
   * position on this sequence (for example, 3 for a dna sequence exon region
   * that is being mapped to a protein sequence). This is optional, since the
   * unit can be usually be inferred from the dictionary type of each sequence
   * involved in the mapping.
   * 
   * @param unit
   *          the value of field 'unit'.
   */
  public void setUnit(final long unit) {
    this._unit = unit;
    this._has_unit = true;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.RangeType
   */
  public static uk.ac.vamsas.objects.core.RangeType unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.RangeType) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.MapRangeType.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
