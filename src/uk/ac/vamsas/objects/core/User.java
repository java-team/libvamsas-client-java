/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class User.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class User extends uk.ac.vamsas.objects.core.AppData implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Field _fullname.
   */
  private java.lang.String _fullname;

  /**
   * Field _organization.
   */
  private java.lang.String _organization;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public User() {
    super();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof User) {

      User temp = (User) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._fullname != null) {
        if (temp._fullname == null)
          return false;
        if (this._fullname != temp._fullname) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._fullname);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._fullname);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._fullname);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._fullname);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._fullname.equals(temp._fullname)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._fullname);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._fullname);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._fullname);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._fullname);
          }
        }
      } else if (temp._fullname != null)
        return false;
      if (this._organization != null) {
        if (temp._organization == null)
          return false;
        if (this._organization != temp._organization) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._organization);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._organization);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._organization);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._organization);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._organization.equals(temp._organization)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._organization);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._organization);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._organization);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._organization);
          }
        }
      } else if (temp._organization != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'fullname'.
   * 
   * @return the value of field 'Fullname'.
   */
  public java.lang.String getFullname() {
    return this._fullname;
  }

  /**
   * Returns the value of field 'organization'.
   * 
   * @return the value of field 'Organization'.
   */
  public java.lang.String getOrganization() {
    return this._organization;
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_fullname != null
        && !org.castor.util.CycleBreaker.startingToCycle(_fullname)) {
      result = 37 * result + _fullname.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_fullname);
    }
    if (_organization != null
        && !org.castor.util.CycleBreaker.startingToCycle(_organization)) {
      result = 37 * result + _organization.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_organization);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
   * Sets the value of field 'fullname'.
   * 
   * @param fullname
   *          the value of field 'fullname'.
   */
  public void setFullname(final java.lang.String fullname) {
    this._fullname = fullname;
  }

  /**
   * Sets the value of field 'organization'.
   * 
   * @param organization
   *          the value of field 'organization'.
   */
  public void setOrganization(final java.lang.String organization) {
    this._organization = organization;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.AppData
   */
  public static uk.ac.vamsas.objects.core.AppData unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.AppData) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.User.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
