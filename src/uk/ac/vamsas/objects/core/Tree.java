/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects.core;

//---------------------------------/
//- Imported classes and packages -/
//---------------------------------/

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Class Tree.
 * 
 * @version $Revision$ $Date: 2007-06-28 14:51:44 +0100 (Thu, 28 Jun 2007)
 *          $
 */
public class Tree extends uk.ac.vamsas.client.Vobject implements
    java.io.Serializable {

  // --------------------------/
  // - Class/Member Variables -/
  // --------------------------/

  /**
   * Primary Key for vamsas object referencing
   */
  private java.lang.String _id;

  /**
   * Field _modifiable.
   */
  private java.lang.String _modifiable;

  /**
   * Field _title.
   */
  private java.lang.String _title;

  /**
   * Field _newickList.
   */
  private java.util.Vector _newickList;

  /**
   * node identity and mapping data between tree representations and vamsas
   * document objects
   */
  private java.util.Vector _treenodeList;

  /**
   * Field _propertyList.
   */
  private java.util.Vector _propertyList;

  /**
   * Field _provenance.
   */
  private uk.ac.vamsas.objects.core.Provenance _provenance;

  // ----------------/
  // - Constructors -/
  // ----------------/

  public Tree() {
    super();
    this._newickList = new java.util.Vector();
    this._treenodeList = new java.util.Vector();
    this._propertyList = new java.util.Vector();
  }

  // -----------/
  // - Methods -/
  // -----------/

  /**
   * 
   * 
   * @param vNewick
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addNewick(final uk.ac.vamsas.objects.core.Newick vNewick)
      throws java.lang.IndexOutOfBoundsException {
    this._newickList.addElement(vNewick);
  }

  /**
   * 
   * 
   * @param index
   * @param vNewick
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addNewick(final int index,
      final uk.ac.vamsas.objects.core.Newick vNewick)
      throws java.lang.IndexOutOfBoundsException {
    this._newickList.add(index, vNewick);
  }

  /**
   * 
   * 
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.addElement(vProperty);
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    this._propertyList.add(index, vProperty);
  }

  /**
   * 
   * 
   * @param vTreenode
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTreenode(final uk.ac.vamsas.objects.core.Treenode vTreenode)
      throws java.lang.IndexOutOfBoundsException {
    this._treenodeList.addElement(vTreenode);
  }

  /**
   * 
   * 
   * @param index
   * @param vTreenode
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void addTreenode(final int index,
      final uk.ac.vamsas.objects.core.Treenode vTreenode)
      throws java.lang.IndexOutOfBoundsException {
    this._treenodeList.add(index, vTreenode);
  }

  /**
   * Method enumerateNewick.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Newick elements
   */
  public java.util.Enumeration enumerateNewick() {
    return this._newickList.elements();
  }

  /**
   * Method enumerateProperty.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Property elements
   */
  public java.util.Enumeration enumerateProperty() {
    return this._propertyList.elements();
  }

  /**
   * Method enumerateTreenode.
   * 
   * @return an Enumeration over all uk.ac.vamsas.objects.core.Treenode elements
   */
  public java.util.Enumeration enumerateTreenode() {
    return this._treenodeList.elements();
  }

  /**
   * Overrides the java.lang.Object.equals method.
   * 
   * @param obj
   * @return true if the objects are equal.
   */
  public boolean equals(final java.lang.Object obj) {
    if (this == obj)
      return true;

    if (super.equals(obj) == false)
      return false;

    if (obj instanceof Tree) {

      Tree temp = (Tree) obj;
      boolean thcycle;
      boolean tmcycle;
      if (this._id != null) {
        if (temp._id == null)
          return false;
        if (this._id != temp._id) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._id);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._id);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._id.equals(temp._id)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._id);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._id);
          }
        }
      } else if (temp._id != null)
        return false;
      if (this._modifiable != null) {
        if (temp._modifiable == null)
          return false;
        if (this._modifiable != temp._modifiable) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._modifiable);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._modifiable);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._modifiable.equals(temp._modifiable)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._modifiable);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._modifiable);
          }
        }
      } else if (temp._modifiable != null)
        return false;
      if (this._title != null) {
        if (temp._title == null)
          return false;
        if (this._title != temp._title) {
          thcycle = org.castor.util.CycleBreaker.startingToCycle(this._title);
          tmcycle = org.castor.util.CycleBreaker.startingToCycle(temp._title);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._title.equals(temp._title)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._title);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._title);
          }
        }
      } else if (temp._title != null)
        return false;
      if (this._newickList != null) {
        if (temp._newickList == null)
          return false;
        if (this._newickList != temp._newickList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._newickList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._newickList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._newickList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._newickList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._newickList.equals(temp._newickList)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._newickList);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._newickList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._newickList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._newickList);
          }
        }
      } else if (temp._newickList != null)
        return false;
      if (this._treenodeList != null) {
        if (temp._treenodeList == null)
          return false;
        if (this._treenodeList != temp._treenodeList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._treenodeList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._treenodeList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._treenodeList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._treenodeList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._treenodeList.equals(temp._treenodeList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._treenodeList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._treenodeList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._treenodeList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._treenodeList);
          }
        }
      } else if (temp._treenodeList != null)
        return false;
      if (this._propertyList != null) {
        if (temp._propertyList == null)
          return false;
        if (this._propertyList != temp._propertyList) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._propertyList);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._propertyList);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._propertyList.equals(temp._propertyList)) {
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(this._propertyList);
              org.castor.util.CycleBreaker
                  .releaseCycleHandle(temp._propertyList);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._propertyList);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._propertyList);
          }
        }
      } else if (temp._propertyList != null)
        return false;
      if (this._provenance != null) {
        if (temp._provenance == null)
          return false;
        if (this._provenance != temp._provenance) {
          thcycle = org.castor.util.CycleBreaker
              .startingToCycle(this._provenance);
          tmcycle = org.castor.util.CycleBreaker
              .startingToCycle(temp._provenance);
          if (thcycle != tmcycle) {
            if (!thcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            }
            ;
            if (!tmcycle) {
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
            }
            ;
            return false;
          }
          if (!thcycle) {
            if (!this._provenance.equals(temp._provenance)) {
              org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
              org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
              return false;
            }
            org.castor.util.CycleBreaker.releaseCycleHandle(this._provenance);
            org.castor.util.CycleBreaker.releaseCycleHandle(temp._provenance);
          }
        }
      } else if (temp._provenance != null)
        return false;
      return true;
    }
    return false;
  }

  /**
   * Returns the value of field 'id'. The field 'id' has the following
   * description: Primary Key for vamsas object referencing
   * 
   * @return the value of field 'Id'.
   */
  public java.lang.String getId() {
    return this._id;
  }

  /**
   * Returns the value of field 'modifiable'.
   * 
   * @return the value of field 'Modifiable'.
   */
  public java.lang.String getModifiable() {
    return this._modifiable;
  }

  /**
   * Method getNewick.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Newick at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Newick getNewick(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._newickList.size()) {
      throw new IndexOutOfBoundsException("getNewick: Index value '" + index
          + "' not in range [0.." + (this._newickList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Newick) _newickList.get(index);
  }

  /**
   * Method getNewick.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Newick[] getNewick() {
    uk.ac.vamsas.objects.core.Newick[] array = new uk.ac.vamsas.objects.core.Newick[0];
    return (uk.ac.vamsas.objects.core.Newick[]) this._newickList.toArray(array);
  }

  /**
   * Method getNewickAsReference.Returns a reference to '_newickList'. No type
   * checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getNewickAsReference() {
    return this._newickList;
  }

  /**
   * Method getNewickCount.
   * 
   * @return the size of this collection
   */
  public int getNewickCount() {
    return this._newickList.size();
  }

  /**
   * Method getProperty.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Property at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Property getProperty(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("getProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Property) _propertyList.get(index);
  }

  /**
   * Method getProperty.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Property[] getProperty() {
    uk.ac.vamsas.objects.core.Property[] array = new uk.ac.vamsas.objects.core.Property[0];
    return (uk.ac.vamsas.objects.core.Property[]) this._propertyList
        .toArray(array);
  }

  /**
   * Method getPropertyAsReference.Returns a reference to '_propertyList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getPropertyAsReference() {
    return this._propertyList;
  }

  /**
   * Method getPropertyCount.
   * 
   * @return the size of this collection
   */
  public int getPropertyCount() {
    return this._propertyList.size();
  }

  /**
   * Returns the value of field 'provenance'.
   * 
   * @return the value of field 'Provenance'.
   */
  public uk.ac.vamsas.objects.core.Provenance getProvenance() {
    return this._provenance;
  }

  /**
   * Returns the value of field 'title'.
   * 
   * @return the value of field 'Title'.
   */
  public java.lang.String getTitle() {
    return this._title;
  }

  /**
   * Method getTreenode.
   * 
   * @param index
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   * @return the value of the uk.ac.vamsas.objects.core.Treenode at the given
   *         index
   */
  public uk.ac.vamsas.objects.core.Treenode getTreenode(final int index)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treenodeList.size()) {
      throw new IndexOutOfBoundsException("getTreenode: Index value '" + index
          + "' not in range [0.." + (this._treenodeList.size() - 1) + "]");
    }

    return (uk.ac.vamsas.objects.core.Treenode) _treenodeList.get(index);
  }

  /**
   * Method getTreenode.Returns the contents of the collection in an Array.
   * <p>
   * Note: Just in case the collection contents are changing in another thread,
   * we pass a 0-length Array of the correct type into the API call. This way we
   * <i>know</i> that the Array returned is of exactly the correct length.
   * 
   * @return this collection as an Array
   */
  public uk.ac.vamsas.objects.core.Treenode[] getTreenode() {
    uk.ac.vamsas.objects.core.Treenode[] array = new uk.ac.vamsas.objects.core.Treenode[0];
    return (uk.ac.vamsas.objects.core.Treenode[]) this._treenodeList
        .toArray(array);
  }

  /**
   * Method getTreenodeAsReference.Returns a reference to '_treenodeList'. No
   * type checking is performed on any modifications to the Vector.
   * 
   * @return a reference to the Vector backing this class
   */
  public java.util.Vector getTreenodeAsReference() {
    return this._treenodeList;
  }

  /**
   * Method getTreenodeCount.
   * 
   * @return the size of this collection
   */
  public int getTreenodeCount() {
    return this._treenodeList.size();
  }

  /**
   * Overrides the java.lang.Object.hashCode method.
   * <p>
   * The following steps came from <b>Effective Java Programming Language
   * Guide</b> by Joshua Bloch, Chapter 3
   * 
   * @return a hash code value for the object.
   */
  public int hashCode() {
    int result = super.hashCode();

    long tmp;
    if (_id != null && !org.castor.util.CycleBreaker.startingToCycle(_id)) {
      result = 37 * result + _id.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_id);
    }
    if (_modifiable != null
        && !org.castor.util.CycleBreaker.startingToCycle(_modifiable)) {
      result = 37 * result + _modifiable.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_modifiable);
    }
    if (_title != null && !org.castor.util.CycleBreaker.startingToCycle(_title)) {
      result = 37 * result + _title.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_title);
    }
    if (_newickList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_newickList)) {
      result = 37 * result + _newickList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_newickList);
    }
    if (_treenodeList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_treenodeList)) {
      result = 37 * result + _treenodeList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_treenodeList);
    }
    if (_propertyList != null
        && !org.castor.util.CycleBreaker.startingToCycle(_propertyList)) {
      result = 37 * result + _propertyList.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_propertyList);
    }
    if (_provenance != null
        && !org.castor.util.CycleBreaker.startingToCycle(_provenance)) {
      result = 37 * result + _provenance.hashCode();
      org.castor.util.CycleBreaker.releaseCycleHandle(_provenance);
    }

    return result;
  }

  /**
   * Method isValid.
   * 
   * @return true if this object is valid according to the schema
   */
  public boolean isValid() {
    try {
      validate();
    } catch (org.exolab.castor.xml.ValidationException vex) {
      return false;
    }
    return true;
  }

  /**
   * 
   * 
   * @param out
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void marshal(final java.io.Writer out)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, out);
  }

  /**
   * 
   * 
   * @param handler
   * @throws java.io.IOException
   *           if an IOException occurs during marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   */
  public void marshal(final org.xml.sax.ContentHandler handler)
      throws java.io.IOException, org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    Marshaller.marshal(this, handler);
  }

  /**
     */
  public void removeAllNewick() {
    this._newickList.clear();
  }

  /**
     */
  public void removeAllProperty() {
    this._propertyList.clear();
  }

  /**
     */
  public void removeAllTreenode() {
    this._treenodeList.clear();
  }

  /**
   * Method removeNewick.
   * 
   * @param vNewick
   * @return true if the object was removed from the collection.
   */
  public boolean removeNewick(final uk.ac.vamsas.objects.core.Newick vNewick) {
    boolean removed = _newickList.remove(vNewick);
    return removed;
  }

  /**
   * Method removeNewickAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Newick removeNewickAt(final int index) {
    java.lang.Object obj = this._newickList.remove(index);
    return (uk.ac.vamsas.objects.core.Newick) obj;
  }

  /**
   * Method removeProperty.
   * 
   * @param vProperty
   * @return true if the object was removed from the collection.
   */
  public boolean removeProperty(
      final uk.ac.vamsas.objects.core.Property vProperty) {
    boolean removed = _propertyList.remove(vProperty);
    return removed;
  }

  /**
   * Method removePropertyAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Property removePropertyAt(final int index) {
    java.lang.Object obj = this._propertyList.remove(index);
    return (uk.ac.vamsas.objects.core.Property) obj;
  }

  /**
   * Method removeTreenode.
   * 
   * @param vTreenode
   * @return true if the object was removed from the collection.
   */
  public boolean removeTreenode(
      final uk.ac.vamsas.objects.core.Treenode vTreenode) {
    boolean removed = _treenodeList.remove(vTreenode);
    return removed;
  }

  /**
   * Method removeTreenodeAt.
   * 
   * @param index
   * @return the element removed from the collection
   */
  public uk.ac.vamsas.objects.core.Treenode removeTreenodeAt(final int index) {
    java.lang.Object obj = this._treenodeList.remove(index);
    return (uk.ac.vamsas.objects.core.Treenode) obj;
  }

  /**
   * Sets the value of field 'id'. The field 'id' has the following description:
   * Primary Key for vamsas object referencing
   * 
   * @param id
   *          the value of field 'id'.
   */
  public void setId(final java.lang.String id) {
    this._id = id;
  }

  /**
   * Sets the value of field 'modifiable'.
   * 
   * @param modifiable
   *          the value of field 'modifiable'.
   */
  public void setModifiable(final java.lang.String modifiable) {
    this._modifiable = modifiable;
  }

  /**
   * 
   * 
   * @param index
   * @param vNewick
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setNewick(final int index,
      final uk.ac.vamsas.objects.core.Newick vNewick)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._newickList.size()) {
      throw new IndexOutOfBoundsException("setNewick: Index value '" + index
          + "' not in range [0.." + (this._newickList.size() - 1) + "]");
    }

    this._newickList.set(index, vNewick);
  }

  /**
   * 
   * 
   * @param vNewickArray
   */
  public void setNewick(final uk.ac.vamsas.objects.core.Newick[] vNewickArray) {
    // -- copy array
    _newickList.clear();

    for (int i = 0; i < vNewickArray.length; i++) {
      this._newickList.add(vNewickArray[i]);
    }
  }

  /**
   * Sets the value of '_newickList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vNewickList
   *          the Vector to copy.
   */
  public void setNewick(final java.util.Vector vNewickList) {
    // copy vector
    this._newickList.clear();

    this._newickList.addAll(vNewickList);
  }

  /**
   * Sets the value of '_newickList' by setting it to the given Vector. No type
   * checking is performed.
   * 
   * @deprecated
   * 
   * @param newickVector
   *          the Vector to set.
   */
  public void setNewickAsReference(final java.util.Vector newickVector) {
    this._newickList = newickVector;
  }

  /**
   * 
   * 
   * @param index
   * @param vProperty
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setProperty(final int index,
      final uk.ac.vamsas.objects.core.Property vProperty)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._propertyList.size()) {
      throw new IndexOutOfBoundsException("setProperty: Index value '" + index
          + "' not in range [0.." + (this._propertyList.size() - 1) + "]");
    }

    this._propertyList.set(index, vProperty);
  }

  /**
   * 
   * 
   * @param vPropertyArray
   */
  public void setProperty(
      final uk.ac.vamsas.objects.core.Property[] vPropertyArray) {
    // -- copy array
    _propertyList.clear();

    for (int i = 0; i < vPropertyArray.length; i++) {
      this._propertyList.add(vPropertyArray[i]);
    }
  }

  /**
   * Sets the value of '_propertyList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vPropertyList
   *          the Vector to copy.
   */
  public void setProperty(final java.util.Vector vPropertyList) {
    // copy vector
    this._propertyList.clear();

    this._propertyList.addAll(vPropertyList);
  }

  /**
   * Sets the value of '_propertyList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param propertyVector
   *          the Vector to set.
   */
  public void setPropertyAsReference(final java.util.Vector propertyVector) {
    this._propertyList = propertyVector;
  }

  /**
   * Sets the value of field 'provenance'.
   * 
   * @param provenance
   *          the value of field 'provenance'.
   */
  public void setProvenance(
      final uk.ac.vamsas.objects.core.Provenance provenance) {
    this._provenance = provenance;
  }

  /**
   * Sets the value of field 'title'.
   * 
   * @param title
   *          the value of field 'title'.
   */
  public void setTitle(final java.lang.String title) {
    this._title = title;
  }

  /**
   * 
   * 
   * @param index
   * @param vTreenode
   * @throws java.lang.IndexOutOfBoundsException
   *           if the index given is outside the bounds of the collection
   */
  public void setTreenode(final int index,
      final uk.ac.vamsas.objects.core.Treenode vTreenode)
      throws java.lang.IndexOutOfBoundsException {
    // check bounds for index
    if (index < 0 || index >= this._treenodeList.size()) {
      throw new IndexOutOfBoundsException("setTreenode: Index value '" + index
          + "' not in range [0.." + (this._treenodeList.size() - 1) + "]");
    }

    this._treenodeList.set(index, vTreenode);
  }

  /**
   * 
   * 
   * @param vTreenodeArray
   */
  public void setTreenode(
      final uk.ac.vamsas.objects.core.Treenode[] vTreenodeArray) {
    // -- copy array
    _treenodeList.clear();

    for (int i = 0; i < vTreenodeArray.length; i++) {
      this._treenodeList.add(vTreenodeArray[i]);
    }
  }

  /**
   * Sets the value of '_treenodeList' by copying the given Vector. All elements
   * will be checked for type safety.
   * 
   * @param vTreenodeList
   *          the Vector to copy.
   */
  public void setTreenode(final java.util.Vector vTreenodeList) {
    // copy vector
    this._treenodeList.clear();

    this._treenodeList.addAll(vTreenodeList);
  }

  /**
   * Sets the value of '_treenodeList' by setting it to the given Vector. No
   * type checking is performed.
   * 
   * @deprecated
   * 
   * @param treenodeVector
   *          the Vector to set.
   */
  public void setTreenodeAsReference(final java.util.Vector treenodeVector) {
    this._treenodeList = treenodeVector;
  }

  /**
   * Method unmarshal.
   * 
   * @param reader
   * @throws org.exolab.castor.xml.MarshalException
   *           if object is null or if any SAXException is thrown during
   *           marshaling
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   * @return the unmarshaled uk.ac.vamsas.objects.core.Tree
   */
  public static uk.ac.vamsas.objects.core.Tree unmarshal(
      final java.io.Reader reader)
      throws org.exolab.castor.xml.MarshalException,
      org.exolab.castor.xml.ValidationException {
    return (uk.ac.vamsas.objects.core.Tree) Unmarshaller.unmarshal(
        uk.ac.vamsas.objects.core.Tree.class, reader);
  }

  /**
   * 
   * 
   * @throws org.exolab.castor.xml.ValidationException
   *           if this object is an invalid instance according to the schema
   */
  public void validate() throws org.exolab.castor.xml.ValidationException {
    org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
    validator.validate(this);
  }

}
