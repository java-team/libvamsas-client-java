/*
 * This file is part of the Vamsas Client version 0.2. 
 * Copyright 2010 by Jim Procter, Iain Milne, Pierre Marguerite, 
 *  Andrew Waterhouse and Dominik Lindner.
 * 
 * Earlier versions have also been incorporated into Jalview version 2.4 
 * since 2008, and TOPALi version 2 since 2007.
 * 
 * The Vamsas Client is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
 * The Vamsas Client is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with the Vamsas Client.  If not, see <http://www.gnu.org/licenses/>.
 */
package uk.ac.vamsas.objects;

import uk.ac.vamsas.client.Vobject;
import uk.ac.vamsas.client.VorbaId;

/**
 * Provides methods to map between VorbaIds and arbitrary object references for
 * use by a vamsas Application when moving between its own datamodel and the
 * Vamsas session objects. The implementing class needs a valid client-document
 * instance if it is expected to be able to register newly created vObjects.
 * Normally this will be the case if the implementing class has been generated
 * by an IClient implementation which will also have passed it a reference to
 * the current valid IClientDocument instance for that application's document
 * access thread. TODO: add remove/clear binding functions - currently you can
 * just pass a null to either argument for bindAppsObjectToVamsasObject to
 * remove the binding from memory.
 * 
 * @author JimP
 * 
 */
public interface IVorbaBinding {
  /**
   * get the Vamsas session object bound to an internal object.
   * 
   * @param appObject
   * @return valid session object or Null.
   */
  Vobject getVamsasObjectFor(Object appObject);

  /**
   * Get the Application's own object bound to an existing Vamsas session object
   * 
   * @param vObject
   *          - object in vamsas document
   * @return apps object bound to the vamsas document object
   */
  Object getAppsObjectFor(uk.ac.vamsas.client.Vobject vObject);

  /**
   * Record a mapping between a vamsas document object and an application's
   * internal object. If either appObject or vObject parameters are null then
   * any existing binding to the non-null object will be deleted
   * 
   * @param appObject
   * @param vObject
   *          - if newly created then it will be registered using the
   *          uk.ac.vamsas.client.IClientDocument.registerObject method.
   */
  void bindAppsObjectToVamsasObject(Object appObject,
      uk.ac.vamsas.client.Vobject vObject);
}
